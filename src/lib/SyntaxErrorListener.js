
export class SyntaxErrorListener {
  static onErrorExecute(aFunction) {
    const listener = new SyntaxErrorListener(window)

    listener.onErrorExecute(aFunction)
  }

  constructor(myWindow) {
    this.myWindow = myWindow

  }

  onErrorExecute(aFunction) {
    this.myWindow.onerror = aFunction
  }
}
