
export class Hooks {
  static empty() {
    return new Hooks()
  }

  constructor() {
    this.hooks = {}
  }

  add(suitName, hook) {
    if (!this.hooks[suitName]) { this.hooks[suitName] = [] }

    this.hooks[suitName].push(hook)
  }

  execute(suitNames) {
    suitNames.forEach(suit => {
      const hooks = this.hooks[suit]

      if (hooks) {
        hooks.forEach(hook => hook())
      }
    })
  }

  clean(suitName) {
    delete this.hooks[suitName]
  }
}
