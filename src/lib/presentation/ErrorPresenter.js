
export class ErrorPresenter {
  static with(text, error) {
    return new ErrorPresenter(text, error)
  }

  constructor(text, error) {
    this.text = text
    this.error = error
  }

  asHtml() {
    return `
      <h3 class="test fails js-test-failing">${this.text}</h3>
      <div class="error-message">${this._buildMainMessage()} ${this._formatTrace()}</div>
    `
  }

  _formatTrace() {
    const stackTrace = this.error.stackTrace()
    const formattedContent = stackTrace.map(this._addErrorTraceClass.bind(this))
    const contentAsString = formattedContent.join('')

    return `<p>${contentAsString}</p>`
  }

  _addErrorTraceClass(line) {
    if (!this._isIncludedInTitle(line)) {
      return `<p class="error-trace">${line}</p>`
    }
  }

  _isIncludedInTitle(line) {
    const title = this._buildMainMessage()

    return title === line
  }

  _buildMainMessage() {
    let message = this.error.rawMessage()

    this._errorArguments().forEach((argument) => {
      const value = this._beautify(argument)

      message = this._interpolate(message, argument, value)
    })

    message = this._addLineBreaksTo(message)

    return `<div class="expected-message">${message}</div>`;
  }

  _errorArguments() {
    return Object.keys(this.error.arguments())
  }

  _interpolate(text, key, value) {
    return text.replace('%' + key, value)
  }

  _addLineBreaksTo(message) {
    return message.replaceAll('%br', '<br />')
  }

  _beautify(name) {
    let argument = this.error.arguments()[name]

    if (!this._isMessage(name)) {
      if (this._isString(argument)) {
        argument = this._addQuotesMarks(argument)
      }
      if (this._isList(argument)) {
        argument = this._showAsList(argument)
      }
    }

    return `<strong>${argument}</strong>`
  }

  _addQuotesMarks(value) {
    return `<pre>"${value}"</pre>`
  }

  _showAsList(value) {
    return JSON.stringify(value)
  }

  _isMessage(value) {
    return value === 'message'
  }

  _isString(value) {
    return typeof value === 'string'
  }

  _isList(value) {
    return Array.isArray(value)
  }
}
