import { ErrorPresenter } from "./ErrorPresenter.js"

export class Drawer {
  static ERROR_BEFORE_ALL_MESSAGE = 'There are errors in beforeAll hook'
  static PASSING_TEST_COUNT = 'Test passed'
  static FAILING_TEST_COUNT = 'Test failed'
  static SKIPPING_TEST_COUNT = 'Test skipped'

  constructor(document) {
    this.document = document

    this.continueDrawing = true
  }

  subjectUnderTest(text) {
    const subjectEmoji = "&#128218;"
    const titleHTML = `<h1 class="suite-description">${subjectEmoji} ${text}</h1>`

    this._appendHTMLToBody(titleHTML)
  }

  testPassing(text) {
    const textHTML = this._htmlTestPassing(text)

    this._appendHTMLToBody(textHTML)
  }

  testFailing(text, error) {
    const failEmoji = "&#x274C;"
    const failText = `${text} ${failEmoji}`
    const textHTML = this._htmlTestFailing(failText, error)

    this._appendHTMLToBody(textHTML)
  }

  skippedTest(text) {
    const skippedEmoji = "&#x1F634;"
    const skippedlText = `${text} ${skippedEmoji}`
    const textHTML = this._htmlSkippedTest(skippedlText)

    this._appendHTMLToBody(textHTML)
  }

  passingTestCount(quantity) {
    const textHTML = this._htmlPassingTestCount(quantity)

    this._updateElementWith('#passing-test-count', textHTML)
  }

  failingTestCount(quantity) {
    const textHTML = this._htmlFailingTestCount(quantity)

    this._updateElementWith('#failing-test-count', textHTML)
  }

  skippingTestCount(quantity) {
    const textHTML = this._htmlSkippingTestCount(quantity)

    this._updateElementWith('#skipping-test-count', textHTML)
  }

  errorInBeforeAll(error) {
    this.testFailing(Drawer.ERROR_BEFORE_ALL_MESSAGE, error)
  }

  syntaxError() {
    const element = this.document.querySelector('#container')
    const emoji = "&#9997;"
    element.innerHTML = `
        <div class="syntax-error">There are a syntax error. ${emoji}</div>
    `;
    this.continueDrawing = false
  }

  removeInstructions() {
    const element = this._instructionsElement()
    element.innerHTML = ""
  }

  drawInstructionsForCreateATest() {
    const element = this._instructionsElement()
    element.innerHTML = `
      <div class="instructions">
        <h1 class="instructions-title">🖊️ How create your first test:</h1>
        <div><span class="instructions-step">1º</span> Create a file called <span class="instructions-higthlight">"Tests.js"</span> in the same folder where you have <span class="instructions-higthlight">"Verano.html"</span></div>
        <div><span class="instructions-step">2º</span> Copy the following code in <span class="instructions-higthlight">"Tests.js"</span> file:</div>
        <pre class="instructions-code">
          <code>
describe("Tests", () => {
  it("fails", () => {
    expect(true).toBe(false)
  })
})
          </code>
        </pre>
      </div>
    `
  }

  _appendHTMLToBody(html) {
    const element = this.document.querySelector('#body')

    if(this.continueDrawing) {
      element.innerHTML += html
    }
  }

  _updateElementWith(selector, html) {
    const element = this.document.querySelector(selector)

    if(this.continueDrawing) {
      element.innerHTML = html
    }
  }

  _htmlTestPassing(text) {
    return `<h3 class="test passes">${text}</h3>`
  }

  _htmlSkippedTest(text) {
    return `<h3 class="test skip">${text}</h3>`
  }

  _htmlTestFailing(text, error) {
    return ErrorPresenter.with(text, error).asHtml()
  }

  _htmlPassingTestCount(quantity) {
    const passedEmoji = "&#127881;"
    return `<div id="passing-test-count">${Drawer.PASSING_TEST_COUNT}: ${quantity} ${passedEmoji}</div>`
  }

  _htmlFailingTestCount(quantity) {
    const failedEmoji= "&#128128;"
    return `<div id="failing-test-count">${Drawer.FAILING_TEST_COUNT}: ${quantity} ${failedEmoji}</div>`
  }

  _htmlSkippingTestCount(quantity) {
    const skippedEmoji= "&#x1F634;"
    return `<div id="skipping-test-count">${Drawer.SKIPPING_TEST_COUNT}: ${quantity} ${skippedEmoji}</div>`
  }

  _instructionsElement() {
    return this.document.querySelector('#instructions')
  }
}
