
export const onLoadMoveDocumentToFirstTestFailing = function() {
  window.addEventListener('load', function() {
    const testFailing = document.querySelector('.js-test-failing')

    if (testFailing) {
      testFailing.scrollIntoView({
        behavior: "smooth",
        block: "center",
        inline: "center"
      })
    }
  })
}
