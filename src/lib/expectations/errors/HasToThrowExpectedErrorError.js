import { ExpectationError } from "./ExpectationError.js"

export const HasToThrowExpectedErrorError = class extends ExpectationError {
  constructor(actual, expected) {
    super()

    this.actual = actual
    this.expected = expected
  }

  rawMessage() {
    return 'Expect to throw error %expected, but throws error %actual'
  }

  arguments() {
    return {
      actual: this.actual.constructor.name,
      expected: this.expected.name
    }
  }
}
