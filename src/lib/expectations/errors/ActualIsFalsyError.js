import { ExpectationError } from "./ExpectationError.js"

export const ActualIsFalsyError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual not to be falsy'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
