import { ExpectationError } from "./ExpectationError.js"

export const CommonMistakeError = class extends ExpectationError {
  rawMessage() {
    return 'You have used the method %tobe, did you mean to use %toBe?'
  }

  arguments() {
    return {
      tobe: 'tobe',
      toBe: 'toBe'
    }
  }
}
