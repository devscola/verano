import { ExpectationError } from "./ExpectationError.js"

export const ExpectationListError = class extends ExpectationError {
  MESSAGE_NEGATIVE = 'The lists are equal'

  constructor(actual, expected, isNegated) {
    super()

    this.actual = actual
    this.expected = expected
    this.isNegated = isNegated
  }

  _selectMessage() {
    let message = this.MESSAGE_POSITIVE

    if (this.isNegated) { message = this.MESSAGE_NEGATIVE }

    return message
  }
}
