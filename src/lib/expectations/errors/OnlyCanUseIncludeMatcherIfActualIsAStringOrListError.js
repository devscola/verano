import { ExpectationListError } from "./ExpectationListError.js"

export const OnlyCanUseIncludeMatcherIfActualIsAStringOrListError = class extends ExpectationListError {
  MESSAGE_POSITIVE = 'The lists have different values'

  rawMessage() {
    return 'Only can use the matcher #include when actual value type are strings or lists'
  }

  arguments() {
    return {}
  }
}
