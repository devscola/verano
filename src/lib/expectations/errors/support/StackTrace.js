import { Config } from "../../../Config.js"

export const StackTrace = class {
  EMPTY_STACK = []

  static removeVeranoFrom(stack) {
    return new StackTrace(stack).removeVerano()
  }

  constructor(stack) {
    this.stack = stack
  }

  removeVerano() {
    if(!this._thereIsStack()) { return this.EMPTY_STACK }
    const stackTrace = this._stackAsArray()

    const stack = Config.customizeStackTrace(stackTrace)

    return stack
  }

  _stackAsArray = () => {
    return this.stack.split("\n")
  }

  _removeVeranoLines = (line) => {
    return !line.includes(this._filenameToRemove())
  }

  _thereIsStack() {
    return !!this.stack
  }
}
