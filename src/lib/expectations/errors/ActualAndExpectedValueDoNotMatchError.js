import { ExpectationError } from "./ExpectationError.js"

export class ActualAndExpectedValueDoNotMatchError extends ExpectationError {
  constructor(actual, expected, isNegated=false) {
    super()

    this.actual = actual
    this.expected = expected
    this.isNegated = isNegated
  }

  rawMessage() {
    return `
      %message%br
      Received: %actual%br
      Expected: %expected%br
    `
  }

  arguments() {
    return {
      actual: this.actual,
      expected: this.expected,
      message: this._buildMessage()
    }
  }

  _buildMessage() {
    let message = 'To be equal'

    if (this.isNegated) { message = 'NOT to be equal' }

    return message
  }
}
