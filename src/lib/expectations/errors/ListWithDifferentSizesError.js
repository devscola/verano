import { ExpectationListError } from "./ExpectationListError.js"

export const ListWithDifferentSizesError = class extends ExpectationListError {
  MESSAGE_POSITIVE = 'The lists have different sizes'

  rawMessage() {
    return this._selectRawMessage()
  }

  arguments() {
    return {
      message: this._selectMessage(),
      actualSize: this.actual.length,
      actual: this.actual,
      expectedSize: this.expected.length,
      expected: this.expected,
    }
  }

  _selectRawMessage() {
    let message = this._positiveRawMessage()

    if (this.isNegated) {
      message = this._negatedRawMessage()
    }

    return message
  }

  _positiveRawMessage() {
    return `
      %message%br
      Received size: %actualSize%br
      Expected size: %expectedSize%br
      %br
      Received list: %actual%br
      Expected list: %expected%br
    `
  }

  _negatedRawMessage() {
    return `
      %message%br
      Received list: %actual%br
      Expected list: %expected%br
    `
  }
}
