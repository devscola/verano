import { ExpectationError } from "./ExpectationError.js"

export const ExpectedIsNotAStringError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return `
      %actual%br
      is not a string, is a %actualType
    `
  }

  arguments() {
    return {
      actual: this.actual,
      actualType: this._actualType()
    }
  }

  _actualType() {
    return typeof this.actual
  }
}
