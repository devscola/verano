import { ExpectationError } from "./ExpectationError.js"

export const ActualIsNotZeroError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual to be Zero'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
