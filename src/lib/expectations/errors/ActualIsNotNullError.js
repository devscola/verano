import { ExpectationError } from "./ExpectationError.js"

export const ActualIsNotNullError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual not to be null'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
