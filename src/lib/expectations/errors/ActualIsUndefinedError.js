import { ExpectationError } from "./ExpectationError.js"

export const ActualIsUndefinedError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual to be undefined'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
