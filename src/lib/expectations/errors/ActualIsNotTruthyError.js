import { ExpectationError } from "./ExpectationError.js"

export const ActualIsNotTruthyError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual to be truthy'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
