import { ExpectationError } from "./ExpectationError.js"

export const ActualIsZeroError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual NOT to be Zero'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
