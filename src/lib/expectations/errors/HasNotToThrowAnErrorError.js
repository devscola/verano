import { ExpectationError } from "./ExpectationError.js"

export const HasNotToThrowAnErrorError = class extends ExpectationError {
  constructor(retievedError) {
    super()

    this.retievedError = retievedError
  }
  rawMessage() {
    return `
      Expect not to throw an error,%br
      but was thrown: %error
    `
  }

  arguments() {
    return {
      error: this._retrivedErrorType()
    }
  }

  _retrivedErrorType() {
    return this.retievedError.constructor.name
  }
}
