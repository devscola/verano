import { ExpectationError } from "./ExpectationError.js"

export const DictionariesAreEqualError = class extends ExpectationError {
  rawMessage() {
    return 'The dictionaries are equal'
  }

  arguments() {
    return {}
  }
}
