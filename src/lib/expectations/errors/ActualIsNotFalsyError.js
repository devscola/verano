import { ExpectationError } from "./ExpectationError.js"

export const ActualIsNotFalsyError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual to be falsy'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
