import { StackTrace } from "./support/StackTrace.js"

export const ExpectationError = class extends Error {
  constructor() {
    super()

    this.isExpectationError = true
  }

  stackTrace() {
    return StackTrace.removeVeranoFrom(this.stack)
  }
}
