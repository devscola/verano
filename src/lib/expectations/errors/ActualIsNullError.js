import { ExpectationError } from "./ExpectationError.js"

export const ActualIsNullError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual to be null'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
