import { ExpectationError } from "./ExpectationError.js"

export const HasNotToThrowASpecificErrorError = class extends ExpectationError {
  constructor(retievedError) {
    super()

    this.retievedError = retievedError
  }
  rawMessage() {
    return `
      Expect not to throw error %error,%br
      but was thrown.
    `
  }

  arguments() {
    return {
      error: this._retrivedErrorType()
    }
  }

  _retrivedErrorType() {
    return this.retievedError.constructor.name
  }
}
