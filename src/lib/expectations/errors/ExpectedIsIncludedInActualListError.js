import { ExpectationError } from "./ExpectationError.js"

export const ExpectedIsIncludedInActualListError = class extends ExpectationError {
  constructor(actual, expected) {
    super()

    this.actual = actual
    this.expected = expected
  }

  rawMessage() {
    return `
      Actual list: %actual%br
      includes: %expected%br
    `
  }

  arguments() {
    return {
      actual: this.actual,
      expected: this.expected
    }
  }
}
