import { ExpectationError } from "./ExpectationError.js"

export const HaveDifferentValuesError = class extends ExpectationError {
  constructor(actual, expected) {
    super()

    this.actual = actual
    this.expected = expected
  }

  rawMessage() {
    return `
      Dictionaries have different values:%br
      Received: %actual%br
      Expected: %expected%br
    `
  }

  arguments() {
    return {
      actual: this._orderedActual(),
      expected: this._orderedExpected()
    }
  }

  _orderedActual() {
    const orderedKeys = this._order(this.actual)

    return orderedKeys
  }

  _orderedExpected() {
    const orderedKeys = this._order(this.expected)

    return orderedKeys
  }

  _order(dictionary) {
    const ordered = {}
    const keys = Object.keys(dictionary)
    const copyDictionary = {...dictionary}
    const orderedKeys = keys.sort()

    orderedKeys.forEach((key) => {
      ordered[key] = copyDictionary[key]

      delete copyDictionary[key]
    })

    return JSON.stringify(ordered)
  }
}
