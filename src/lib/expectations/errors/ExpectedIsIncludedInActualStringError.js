import { ExpectationError } from "./ExpectationError.js"

export const ExpectedIsIncludedInActualStringError = class extends ExpectationError {
  constructor(actual, expected) {
    super()

    this.actual = actual
    this.expected = expected
  }

  rawMessage() {
    return `
      Actual string: %actual%br
      includes substring: %expected%br
    `
  }

  arguments() {
    return {
      actual: this.actual,
      expected: this.expected
    }
  }
}
