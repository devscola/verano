import { ExpectationError } from "./ExpectationError.js"

export const ActualIsNotUndefinedError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual NOT to be undefined'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
