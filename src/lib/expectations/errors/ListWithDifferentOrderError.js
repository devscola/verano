import { ExpectationListError } from "./ExpectationListError.js"

export const ListWithDifferentOrderError = class extends ExpectationListError {
  MESSAGE_POSITIVE = 'The lists have different order'

  rawMessage() {
    return `
      %message%br
      Received list: %actual%br
      Expected list: %expected%br
    `
  }

  arguments() {
    return {
      message: this._selectMessage(),
      actual: this.actual,
      expected: this.expected
    }
  }
}
