import { ExpectationError } from "./ExpectationError.js"

export const HasToThrowAnErrorError = class extends ExpectationError {
  rawMessage() {
    return 'Expect to raise an error, but nothing happens'
  }

  arguments() {
    return {}
  }
}
