import { ExpectationListError } from "./ExpectationListError.js"

export const ListWithDifferentValuesError = class extends ExpectationListError {
  MESSAGE_POSITIVE = 'The lists have different values'

  rawMessage() {
    return `
      %message%br
      Received ordered list: %actual%br
      Expected ordered list: %expected%br
    `
  }

  arguments() {
    return {
      message: this._selectMessage(),
      actual: this.actual,
      expected: this.expected
    }
  }
}
