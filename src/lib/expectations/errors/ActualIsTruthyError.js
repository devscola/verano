import { ExpectationError } from "./ExpectationError.js"

export const ActualIsTruthyError = class extends ExpectationError {
  constructor(actual) {
    super()

    this.actual = actual
  }

  rawMessage() {
    return 'Expect %actual not to be truthy'
  }

  arguments() {
    return {
      actual: this.actual
    }
  }
}
