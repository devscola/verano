import { ActualIsNotNullError } from "./errors/ActualIsNotNullError.js"
import { ActualIsNullError } from "./errors/ActualIsNullError.js"

export const ExpectNull = class {
  constructor(actual) {
    this.conditionForThrowError = this._notEqual
    this.isNegated = false

    this.actual = actual
  }

  get not() {
    this.conditionForThrowError = this._equal
    this.isNegated = true

    return this
  }

  toBeNull() {
    if (this.conditionForThrowError()) {
      throw this._expectationError()
    }
  }

  _expectationError() {
    if (this.isNegated) {
      return new ActualIsNotNullError(this.actual)
    } else {
      return new ActualIsNullError(this.actual)
    }
  }

  _notEqual() {
    return (this.actual !== null)
  }

  _equal() {
    return (this.actual === null)
  }
}
