import { ActualIsFalsyError } from "./errors/ActualIsFalsyError.js"
import { ActualIsNotFalsyError } from "./errors/ActualIsNotFalsyError.js"

export const ExpectToBeFalsy = class {
  constructor(actual) {
    this.isNegated = false

    this.actual = actual
  }

  get not() {
    this.isNegated = true

    return this
  }

  toBeFalsy() {
    if (this.isNegated) {
      this._throwAnErrorIfActualIsFalsy()
    } else {
      this._throwAnErrorIfActualIsNotFalsy()
    }
  }

  _throwAnErrorIfActualIsFalsy() {
    if (!this.actual) {
      throw new ActualIsFalsyError(this.actual)
    }
  }

  _throwAnErrorIfActualIsNotFalsy() {
    if (this.actual) {
      throw new ActualIsNotFalsyError(this.actual)
    }
  }
}
