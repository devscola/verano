import { DictionariesAreEqualError } from "./errors/DictionariesAreEqualError.js"
import { HaveDifferentKeysError } from "./errors/HaveDifferentKeysError.js"
import { HaveDifferentKeysQuantityError } from "./errors/HaveDifferentKeysQuantityError.js"
import { HaveDifferentValuesError } from "./errors/HaveDifferentValuesError.js"

export const ExpectDictionary = class {
  constructor(actual) {
    this.hasToBeDifferentDictionaries = false
    this.actual = actual
  }

  get not() {
    this.hasToBeDifferentDictionaries = true

    return this
  }

  toBe(expected) {
    const haveDifferentQuantityOfKeys = this._haveDifferentQuantityOfKeys(this.actual, expected)
    const haveDifferentKeys = this._haveDifferentKeys(this.actual, expected)
    const haveDifferentValues = this._haveDifferentValues(this.actual, expected)

    if (this.hasToBeDifferentDictionaries) {
      const areTheSameDictionary = (!haveDifferentQuantityOfKeys && !haveDifferentKeys && !haveDifferentValues)

      if (areTheSameDictionary) {
        throw new DictionariesAreEqualError()
      }
    } else {
      if (haveDifferentQuantityOfKeys) {
        throw new HaveDifferentKeysQuantityError(this.actual, expected)
      }

      if (haveDifferentKeys) {
        throw new HaveDifferentKeysError(this.actual, expected)
      }

      if(haveDifferentValues) {
        throw new HaveDifferentValuesError(this.actual, expected)
      }
    }
  }

  _haveDifferentQuantityOfKeys(actual, expected) {
    const actualKeys = Object.keys(actual)
    const expectedKeys = Object.keys(expected)

    const haveDifferentQuantityOfKeys = (actualKeys.length !== expectedKeys.length)

    return haveDifferentQuantityOfKeys
  }

  _haveDifferentKeys(actual, expected) {
    const actualKeys = Object.keys(actual)
    const expectedKeys = Object.keys(expected)
    let haveDifferentKeys = false

    actualKeys.forEach(key => {
      if(!expectedKeys.includes(key)) {
        haveDifferentKeys = true
      }
    })
    expectedKeys.forEach(key => {
      if(!actualKeys.includes(key)) {
        haveDifferentKeys = true
      }
    })

    return haveDifferentKeys
  }

  _haveDifferentValues(actual, expected) {
    let haveDifferentValues = false

    for(let [index, value] of Object.entries(actual)) {
      if(expected[index] !== value) {
        haveDifferentValues = true
      }
    }
    for(let [index, value] of Object.entries(expected)) {
      if(actual[index] !== value) {
        haveDifferentValues = true
      }
    }

    return haveDifferentValues
  }

  _equal(value, anotherValue) {
    return (value === anotherValue)
  }
}
