import { ExpectedIsIncludedInActualListError } from "./errors/ExpectedIsIncludedInActualListError.js"
import { ExpectedIsNotAListError } from "./errors/ExpectedIsNotAListError.js"
import { ExpectedIsNotIncludedInActualListError } from "./errors/ExpectedIsNotIncludedInActualListError.js"

export const ExpectToIncludeElement = class {
  constructor(actual) {
    this._throwErrorIfIsNotAList(actual)
    this.isNegated = false

    this.actual = actual
  }

  get not() {
    this.isNegated = true

    return this
  }

  include(expected) {
    if (this.isNegated) {
      this._throwAnErrorIfActualIncludes(expected)
    } else {
      this._throwAnErrorIfActualDoesNotInclude(expected)
    }
  }

  _throwAnErrorIfActualIncludes(expected) {
    if (this._isActualIncluding(expected)) {
      throw new ExpectedIsIncludedInActualListError(this.actual, expected)
    }
  }

  _throwAnErrorIfActualDoesNotInclude(expected) {
    if (this._isNotActualIncluding(expected)) {
      throw new ExpectedIsNotIncludedInActualListError(this.actual, expected)
    }
  }

  _throwErrorIfIsNotAList(actual) {
    if (this._isNotAList(actual)) {
      throw new ExpectedIsNotAListError(actual)
    }
  }

  _isNotAList(actual) {
    return (!Array.isArray(actual))
  }

  _isActualIncluding(expected) {
    return (this.actual.includes(expected))
  }

  _isNotActualIncluding(expected) {
    return (!this.actual.includes(expected))
  }
}
