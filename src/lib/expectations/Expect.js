import { ActualAndExpectedValueDoNotMatchError } from "./errors/ActualAndExpectedValueDoNotMatchError.js"
import { CommonMistakeError } from "./errors/CommonMistakeError.js"
import { OnlyCanUseIncludeMatcherIfActualIsAStringOrListError } from "./errors/OnlyCanUseIncludeMatcherIfActualIsAStringOrListError.js"
import { ExpectAnError } from "./ExpectAnError.js"
import { ExpectDictionary } from "./ExpectDictionary.js"
import { ExpectLists } from "./ExpectLists.js"
import { ExpectNull } from "./ExpectNull.js"
import { ExpectToBeFalsy } from "./ExpectToBeFalsy.js"
import { ExpectToBeUndefined } from "./ExpectToBeUndefined.js"
import { ExpectToBeZero } from "./ExpectToBeZero.js"
import { ExpectToIncludeElement } from "./ExpectToIncludeElement.js"
import { ExpectToIncludeSubstring } from "./ExpectToIncludeSubstring.js"
import { ExpectTruth } from "./ExpectTruth.js"

export class Expect {
  constructor(actual) {
    this.actual = actual
    this.conditionForThrowError = this._notEqual
    this.isNegated = false
  }

  get not() {
    this.conditionForThrowError = this._equal
    this.isNegated = true

    return this
  }

  toBe(expected) {
    if (this._hasToCompareLists(expected)) {
      this._compareListsActualAnd(expected)
    } else if (this._hasToCompareDictionaries(expected)) {
      this._compareDictionariesActualAnd(expected)
    } else if (this.conditionForThrowError(this.actual, expected)) {
      throw new ActualAndExpectedValueDoNotMatchError(this.actual, expected, this.isNegated)
    }
  }

  toBeTruthy() {
    if (this.isNegated) {
      new ExpectTruth(this.actual).not.toBeTruthy()
    } else {
      new ExpectTruth(this.actual).toBeTruthy()
    }
  }

  toBeFalsy() {
    if (this.isNegated) {
      new ExpectToBeFalsy(this.actual).not.toBeFalsy()
    } else {
      new ExpectToBeFalsy(this.actual).toBeFalsy()
    }
  }

  toBeNull() {
    if (this.isNegated) {
      new ExpectNull(this.actual).not.toBeNull()
    } else {
      new ExpectNull(this.actual).toBeNull()
    }
  }

  toThrowAnError(expected) {
    if (this.isNegated) {
      new ExpectAnError(this.actual).not.toThrowAnError(expected)
    } else {
      new ExpectAnError(this.actual).toThrowAnError(expected)
    }
  }

  toBeUndefined() {
    if (this.isNegated) {
      new ExpectToBeUndefined(this.actual).not.toBeUndefined()
    } else {
      new ExpectToBeUndefined(this.actual).toBeUndefined()
    }
  }

  toBeZero() {
    if(this.isNegated) {
      new ExpectToBeZero(this.actual).not.toBeZero()
    } else {
      new ExpectToBeZero(this.actual).toBeZero()
    }
  }

  include(expected) {
    if (this._isActualAString()) {
      this._checkIfActualIncludesSubstring(expected)
    } else if (this._isActualAList()) {
      this._checkIfActualIncludesElement(expected)
    } else {
      throw new OnlyCanUseIncludeMatcherIfActualIsAStringOrListError()
    }
  }

  tobe(_) {
    throw new CommonMistakeError()
  }

  _checkIfActualIncludesSubstring(expected) {
    if (this.isNegated) {
      new ExpectToIncludeSubstring(this.actual).not.include(expected)
    } else {
      new ExpectToIncludeSubstring(this.actual).include(expected)
    }
  }

  _checkIfActualIncludesElement(expected) {
    if (this.isNegated) {
      new ExpectToIncludeElement(this.actual).not.include(expected)
    } else {
      new ExpectToIncludeElement(this.actual).include(expected)
    }
  }

  _compareListsActualAnd(expected) {
    if (this.isNegated) {
      new ExpectLists(this.actual).not.toBe(expected)
    } else {
      new ExpectLists(this.actual).toBe(expected)
    }
  }

  _compareDictionariesActualAnd(expected) {
    if (this.isNegated) {
      new ExpectDictionary(this.actual).not.toBe(expected)
    } else {
      new ExpectDictionary(this.actual).toBe(expected)
    }
  }

  _hasToCompareLists(expected) {
    return (Array.isArray(expected) && this._isActualAList())
  }

  _isActualAList() {
    return Array.isArray(this.actual)
  }

  _isActualAString() {
    return (typeof this.actual === 'string')
  }

  _hasToCompareDictionaries(expected) {
    const expectedIsADictionary = this._isADictionary(expected)
    const actualIsADictionary = this._isADictionary(this.actual)

    return expectedIsADictionary && actualIsADictionary
  }

  _isADictionary(value) {
    if (value === null) { return false }

    return (typeof value === 'object' && value.constructor === Object)
  }

  _equal(value, anotherValue) {
    return value === anotherValue
  }

  _notEqual(value, anotherValue) {
    return value !== anotherValue
  }
}
