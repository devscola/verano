import { ActualIsNotZeroError } from "./errors/ActualIsNotZeroError.js"
import { ActualIsZeroError } from "./errors/ActualIsZeroError.js"

export const ExpectToBeZero = class {
  constructor(actual) {
    this.actual = actual

    this.isNegated = false
  }

  get not() {
    this.isNegated = true

    return this
  }

  toBeZero() {
    if(this.isNegated) {
     this._isNotEqualToZero()
    } else {
      this._isEqualToZero()
    }
  }

  _isEqualToZero() {
    if(this.actual !== 0) {
      throw new ActualIsNotZeroError(this.actual)
    }
  }

  _isNotEqualToZero() {
    if(this.actual === 0) {
      throw new ActualIsZeroError(this.actual)
    }
  }
}
