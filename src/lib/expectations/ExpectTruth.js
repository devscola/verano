import { ActualIsNotTruthyError } from "./errors/ActualIsNotTruthyError.js"
import { ActualIsTruthyError } from "./errors/ActualIsTruthyError.js"

export const ExpectTruth = class {
  constructor(actual) {
    this.isNegated = false

    this.actual = actual
  }

  get not() {
    this.isNegated = true

    return this
  }

  toBeTruthy() {
    if (this.isNegated) {
      this._throwAnErrorIfActualIsTruthy()
    } else {
      this._throwAnErrorIfActualIsNotTruthy()
    }
  }

  _throwAnErrorIfActualIsNotTruthy() {
    if (!this.actual) {
      throw new ActualIsNotTruthyError(this.actual)
    }
  }

  _throwAnErrorIfActualIsTruthy() {
    if (this.actual) {
      throw new ActualIsTruthyError(this.actual)
    }
  }
}
