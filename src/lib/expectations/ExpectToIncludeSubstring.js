import { ExpectedIsIncludedInActualStringError } from "./errors/ExpectedIsIncludedInActualStringError.js"
import { ExpectedIsNotAStringError } from "./errors/ExpectedIsNotAStringError.js"
import { ExpectedIsNotIncludedInActualStringError } from "./errors/ExpectedIsNotIncludedInActualStringError.js"

export const ExpectToIncludeSubstring = class {
  constructor(actual) {
    this._throwAnErrorIfIsNotString(actual)
    this.isNegated = false

    this.actual = actual
  }

  get not() {
    this.isNegated = true

    return this
  }

  include(expected) {
    if (this.isNegated) {
      this._throwAnErrorIfActualIncludes(expected)
    } else {
      this._throwAnErrorIfActualDoesNotIncludes(expected)
    }
  }

  _throwAnErrorIfIsNotString(value) {
    const isNotValueAString = (typeof value !== 'string')

    if (isNotValueAString) {
      throw new ExpectedIsNotAStringError(value)
    }
  }

  _throwAnErrorIfActualIncludes(expected) {
    if (this._isActualIncluding(expected)) {
      throw new ExpectedIsIncludedInActualStringError(this.actual, expected)
    }
  }

  _throwAnErrorIfActualDoesNotIncludes(expected) {
    if (this._isNotActualIncluding(expected)) {
      throw new ExpectedIsNotIncludedInActualStringError(this.actual, expected)
    }
  }

  _isActualIncluding(expected) {
    return (this.actual.includes(expected))
  }

  _isNotActualIncluding(expected) {
    return (!this.actual.includes(expected))
  }
}
