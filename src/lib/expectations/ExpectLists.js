import { ListWithDifferentOrderError } from "./errors/ListWithDifferentOrderError.js"
import { ListWithDifferentSizesError } from "./errors/ListWithDifferentSizesError.js"
import { ListWithDifferentValuesError } from "./errors/ListWithDifferentValuesError.js"

export class ExpectLists {
  constructor(actual) {
    this.actual = actual
    this.conditionForThrowError = this._notEqual
    this.isNegated = false
  }

  get not() {
    this.conditionForThrowError = this._equal
    this.isNegated = true

    return this
  }

  toBe(expected) {
    if (this._conditionForThrowErrorBySizes(expected)) { throw new ListWithDifferentSizesError(this.actual, expected, this.isNegated) }
    if (this._conditionForThrowErrorByValues(expected)) { throw new ListWithDifferentValuesError(this.actual, expected, this.isNegated) }
    if (this._conditionForThrowErrorByOrders(expected)) { throw new ListWithDifferentOrderError(this.actual, expected, this.isNegated) }
  }

  _conditionForThrowErrorBySizes(expected) {
    return this.conditionForThrowError(this.actual.length, expected.length)
  }

  _conditionForThrowErrorByValues(expected) {
    const clonedActual = [...this.actual]
    const cloneExpected = [...expected]
    const sortedActual = clonedActual.sort()
    const sortedExpected = cloneExpected.sort()

    return this.conditionForThrowError(this._toString(sortedActual), this._toString(sortedExpected))
  }

  _conditionForThrowErrorByOrders(expected) {
    return this.conditionForThrowError(this._toString(this.actual), this._toString(expected))
  }

  _toString(value) {
    return JSON.stringify(value)
  }

  _equal(value, anotherValue) {
    return value === anotherValue
  }

  _notEqual(value, anotherValue) {
    return value !== anotherValue
  }
}
