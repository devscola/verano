import { ActualIsNotUndefinedError } from "./errors/ActualIsNotUndefinedError.js"
import { ActualIsUndefinedError } from "./errors/ActualIsUndefinedError.js"

export const ExpectToBeUndefined = class {
  constructor(actual) {
    this.isNegated = false

    this.actual = actual
  }

  get not() {
    this.isNegated = true

    return this
  }

  toBeUndefined() {
    if (this.isNegated) {
      this._throwAnErrorIfIsUndefined()
    } else {
      this._throwAnErrorIfIsNotUndefined()
    }
  }

  _throwAnErrorIfIsUndefined() {
    if(this._isUndefined()) {
      throw new ActualIsUndefinedError(this.actual)
    }
  }

  _throwAnErrorIfIsNotUndefined() {
    if(this._isNotUndefined()) {
      throw new ActualIsNotUndefinedError(this.actual)
    }
  }

  _isUndefined() {
    return (this.actual === undefined)
  }

  _isNotUndefined() {
    return (this.actual !== undefined)
  }
}
