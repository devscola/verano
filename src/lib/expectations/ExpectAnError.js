import { HasNotToThrowAnErrorError } from "./errors/HasNotToThrowAnErrorError.js"
import { HasNotToThrowASpecificErrorError } from "./errors/HasNotToThrowASpecificErrorError.js"
import { HasToThrowAnErrorError } from "./errors/HasToThrowAnErrorError.js"
import { HasToThrowExpectedErrorError } from "./errors/HasToThrowExpectedErrorError.js"

export const ExpectAnError = class {
  constructor(act) {
    this.isNegated = false

    this.act = act
  }

  get not() {
    this.isNegated = true

    return this
  }

  toThrowAnError(expected) {
    if (this.isNegated) {
      this._hasNotToThrowError(expected)
    } else {
      this._hasToThrowError(expected)
    }
  }

  _hasNotToThrowError(expected) {
    try {
      this.act()
    } catch (error) {
      if (expected) {
        throw new HasNotToThrowASpecificErrorError(error)
      } else {
        throw new HasNotToThrowAnErrorError(error)
      }
    }
  }

  _hasToThrowError(expected) {
    let errorWasThrowed = false

    try {
      this.act()
    } catch (error) {
      errorWasThrowed = true

      if (expected) {
        const isThrowingSpecificError = (error instanceof expected)

        if (!isThrowingSpecificError) {
          throw new HasToThrowExpectedErrorError(error, expected)
        }
      }
    }

    if (!errorWasThrowed) {
      throw new HasToThrowAnErrorError()
    }
  }
}
