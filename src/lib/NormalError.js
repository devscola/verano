import { StackTrace } from "./expectations/errors/support/StackTrace.js"

export class NormalError extends Error {
  constructor(error) {
    super()

    this.error = error
  }

  stackTrace() {
    return StackTrace.removeVeranoFrom(this._stack())
  }

  rawMessage() {
    return this.error.toString()
  }

  arguments() {
    return {}
  }

  _stack() {
    if(this._hasErrorStack()) {
      return this.error.stack
    } else {
      return this.stack
    }
  }

  _hasErrorStack() {
    return (this.error instanceof Error)
  }
}
