import { onLoadMoveDocumentToFirstTestFailing } from './presentation/onLoadMoveDocumentToFirstTestFailing.js'
import { SyntaxErrorListener } from './SyntaxErrorListener.js'
import { Drawer } from './presentation/Drawer.js'
import { Hooks } from './Hooks.js'
import { NormalError } from './NormalError.js'
import { Expect } from './expectations/Expect.js'

window.drawer = new Drawer(document)
let currentTestSuit = null
let testSuitsRunning = []
const beforeEachHooks = Hooks.empty()
const afterAllHooks = Hooks.empty()
const afterEachHooks = Hooks.empty()
let passingTestCount = 0
let failingTestCount = 0
let skippingTestCount = 0

SyntaxErrorListener.onErrorExecute(() => {
  drawer.syntaxError()
})

const beforeAll = (hook) => {
  try {
    hook()
  } catch(error) {
    drawer.errorInBeforeAll(normalize(error))
  }
}

const beforeEach = (hook) => {
  beforeEachHooks.add(currentTestSuit, hook)
}

const afterAll = (hook) => {
  afterAllHooks.add(currentTestSuit, hook)
}

const afterEach = (hook) => {
  afterEachHooks.add(currentTestSuit, hook)
}

const cleanHooks = () => {
  beforeEachHooks.clean(currentTestSuit)
  afterAllHooks.clean(currentTestSuit)
  afterEachHooks.clean(currentTestSuit)
}

const markAsRunning = (subjectUnderTest) => {
  currentTestSuit = subjectUnderTest
  testSuitsRunning.push(subjectUnderTest)
}

const unmarkAsRunning = (subjectUnderTest) => {
  currentTestSuit = null
  testSuitsRunning = testSuitsRunning.filter(suit => suit !== subjectUnderTest)
}

const NO_TEST_SUIT_PROVIDED = () => {}
const describe = (subjectUnderTest, testSuit=NO_TEST_SUIT_PROVIDED) => {
  try {
    drawer.subjectUnderTest(subjectUnderTest)
    markAsRunning(subjectUnderTest)

    testSuit()

    afterAllHooks.execute(testSuitsRunning)
    cleanHooks()
    unmarkAsRunning(subjectUnderTest)
  } catch (error) {
    drawer.testFailing('An error is thrown in this describe', normalize(error))
  }
}

const NO_TEST_PROVIDED = () => {}
const it = (description, test=NO_TEST_PROVIDED) => {
  try {
    drawer.removeInstructions()
    beforeEachHooks.execute(testSuitsRunning)

    test()

    passingTestCount ++
    drawer.passingTestCount(passingTestCount)
    afterEachHooks.execute(testSuitsRunning)
    drawer.testPassing(description)
  } catch (error) {
    failingTestCount ++
    drawer.failingTestCount(failingTestCount)
    drawer.testFailing(description, normalize(error))
  }
}

const xit = (description, _) => {
  drawer.removeInstructions()
  drawer.skippedTest(description)

  skippingTestCount++
  drawer.skippingTestCount(skippingTestCount)
}

const expect = (actual) => {
  return new Expect(actual)
}

export const normalize = (error) => {
  if (error.isExpectationError) { return error }

  return new NormalError(error)
}

export const Verano = () => {
  window.drawer = drawer
  window.currentTestSuit = currentTestSuit
  window.testSuitsRunning = testSuitsRunning
  window.beforeEachHooks = beforeEachHooks
  window.afterAllHooks = afterAllHooks
  window.afterEachHooks = afterEachHooks
  window.passingTestCount = passingTestCount
  window.failingTestCount = failingTestCount
  window.skippingTestCount = skippingTestCount
  window.beforeAll = beforeAll
  window.beforeEach = beforeEach
  window.afterAll = afterAll
  window.afterEach = afterEach
  window.describe = describe
  window.it = it
  window.xit = xit
  window.expect = expect

  onLoadMoveDocumentToFirstTestFailing()
  drawer.drawInstructionsForCreateATest()
}

Verano()
