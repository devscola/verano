
const customizeStackTrace = (stackTrace) => {
  let stack = stackTrace.filter((line) => !line.includes("Verano.html"))

  return stack
}

export class Config {
  static customizeStackTrace = customizeStackTrace
}
