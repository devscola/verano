FROM node:16.18.0-slim

WORKDIR /opt/verano

COPY package* ./

RUN npm install

COPY . ./

CMD ["npm", "run", "serve"]
