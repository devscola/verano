
class VeranoPage {
  static visit() {
    return new VeranoPage()
  }

  constructor() {
    cy.visit('./tests/ci/UserUsageTests.html')
  }

  includesTestPassing() {
    cy.get('.test.passes')

    return true
  }

  includesTestFailing() {
    cy.get('.test.fails')

    return true
  }

  includesSkippedTest() {
    cy.get('.test.skip')

    return true
  }

  includesErrorInBeforeAll() {
    cy.contains('There are errors in beforeAll hook')

    return true
  }

  includesPassingTestCount() {
    cy.contains('Test passed: 1')

    return true
  }

  includesFailingTestCount() {
    cy.contains('Test failed: 3')

    return true
  }

  includesSkippingTestCount() {
    cy.contains('Test skipped: 1')

    return true
  }

  hideInstructions() {
    cy.get('#instructions').should('have.text', '')

    return true
  }

  contains(text) {
    cy.contains(text)

    return true
  }
}

module.exports = { VeranoPage }
