
describe('Unit tests', () => {
  it('has no failures', () => {
    cy.visit('./TestRunner.html')

    cy.get('.error-message').should('not.exist')
    cy.get('.fails').should('not.exist')
    cy.get('.skips').should('not.exist')
    cy.contains('are drawn (beforeAll failures)')
    cy.contains('Test passed: ')
  })
})
