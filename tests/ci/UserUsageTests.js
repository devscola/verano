
describe('Production Tests', () => {
  it('pass', () => {
    expect(true).toBe(true)
  })

  it('fails', () => {
    expect(false).toBe(true)
  })

  it('fails when is negated', () => {
    expect(false).not.toBe(false)
  })

  it('fails for not expection errors', () => {

    UNDEFINED_CONSTANT()

    expect(false).toBe(true)
  })

  xit('skips this test')
})

describe('beforeAll failures', () => {
  beforeAll(() => {
    fails()
  })
})
