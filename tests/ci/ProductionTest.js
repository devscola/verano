const { VeranoPage } = require('./support/VeranoPage')

describe('End-to-End Tests', () => {
  let page

  beforeEach(() => {
    page = VeranoPage.visit()
  })

  it("shows instructions where there are not tests", () => {
    cy.visit("../../src/index.html")

    expect(page.contains("🖊️ How create your first test:")).to.eq(true)
  })

  it('does not show instructions', () => {
    expect(page.hideInstructions()).to.eq(true)
  })

  it('has a test passing', () => {
    expect(page.includesTestPassing()).to.eq(true)
  })

  it('has a test failing by expectation', () => {
    expect(page.includesTestFailing()).to.eq(true)
    expect(page.contains('To be equal Received: false Expected: true')).to.eq(true)
  })

  it('has a test failing by expectation when is negated', () => {
    expect(page.includesTestFailing()).to.eq(true)
    expect(page.contains('NOT to be equal Received: false Expected: false')).to.eq(true)
  })

  it('has a test failing by normal error', () => {
    expect(page.includesTestFailing()).to.eq(true)
    expect(page.contains('ReferenceError: UNDEFINED_CONSTANT is not defined')).to.eq(true)
  })

  it('has a skipped test', () => {
    expect(page.includesSkippedTest()).to.eq(true)
  })

  it('has beforeAll failure', () => {
    expect(page.includesErrorInBeforeAll()).to.eq(true)
  })

  it('includes the test counts', () => {
    expect(page.includesPassingTestCount()).to.eq(true)
    expect(page.includesFailingTestCount()).to.eq(true)
    expect(page.includesSkippingTestCount()).to.eq(true)
  })

  it('shows instructions for create the first test when there are no tests yet', () => {

    cy.visit('./src/index.html')

    cy.contains('🖊️ How create your first test:')
  })
})
