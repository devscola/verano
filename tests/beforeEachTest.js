
let hasBeenExecutedBeforeEach = false

describe('beforeEach', () => {
  beforeEach(() => {
    hasBeenExecutedBeforeEach = true
  })

  it('executes before every test', () => {
    expect(hasBeenExecutedBeforeEach).toBe(true)
    hasBeenExecutedBeforeEach = false
  })

  it('executes again', () => {
    expect(hasBeenExecutedBeforeEach).toBe(true)
  })
})

describe('beforeEachHook', () => {
  it('is cleaned after test suit', () => {
    hasBeenExecutedBeforeEach = false

    beforeEachHooks.execute(testSuitsRunning)

    expect(hasBeenExecutedBeforeEach).toBe(false)
  })
})

describe('nested beforeEachs', () => {
  let values = null

  beforeEach(() => {
    values = ['notNested']
  })

  describe('executes its own and parent beforeEachs', () => {
    beforeEach(() => {
      values.push('firstNested')
    })
    beforeEach(() => {
      values.push('anotherfirstNested')
    })

    it('has been executed only expected beforeEach', () => {
      expect(values.length).toBe(3)
      expect(values[0]).toBe('notNested')
      expect(values[1]).toBe('firstNested')
      expect(values[2]).toBe('anotherfirstNested')
    })
  })

  describe('executes its own and parent beforeEachs', () => {
    beforeEach(() => {
      values.push('repeatedNested')
    })

    it('does not executes beforeEachs from describes with the same name', () => {
      expect(values.length).toBe(2)
      expect(values[0]).toBe('notNested')
      expect(values[1]).toBe('repeatedNested')
    })
  })

  describe('does not execute brother beforeEach', () => {
    beforeEach(() => {
      values.push('secondNested')
    })

    it('has been executed only expected beforeEach', () => {
      expect(values.length).toBe(2)
      expect(values[0]).toBe('notNested')
      expect(values[1]).toBe('secondNested')
    })
  })
})
