import { Hooks } from "../src/lib/Hooks.js"
import { CallbackSpy } from "./support/CallbackSpy.js"

describe('Hooks', () => {
  let firstHook, secondHook

  beforeEach(() => {
    firstHook = new CallbackSpy()
    secondHook = new CallbackSpy()
  })

  it("executes all Suit's hooks", () => {
    const suitName = 'someSuitName'
    const suitNames = [suitName]
    const hooks = Hooks.empty()
    hooks.add(suitName, firstHook.execute)
    hooks.add(suitName, secondHook.execute)

    hooks.execute(suitNames)

    expect(firstHook.hasBeenExecuted()).toBe(true)
    expect(secondHook.hasBeenExecuted()).toBe(true)
  })

  it("cleans all Suit's hooks", () => {
    const suitName = 'someSuitName'
    const suitNames = [suitName]
    const hooks = Hooks.empty()
    hooks.add(suitName, firstHook.callback)
    hooks.add(suitName, secondHook.callback)

    hooks.clean(suitName)

    hooks.execute(suitNames)
    expect(firstHook.hasBeenExecuted()).toBe(false)
    expect(secondHook.hasBeenExecuted()).toBe(false)
  })
})
