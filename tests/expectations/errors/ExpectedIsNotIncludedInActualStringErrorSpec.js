import { ExpectedIsNotIncludedInActualStringError } from "../../../src/lib/expectations/errors/ExpectedIsNotIncludedInActualStringError.js"

describe('ExpectedIsNotIncludedInActualStringError', () => {
  const actualString = 'Actual String'
  const expectedSubstring = 'expectedSubstring'

  it('retrieves the error message', () => {

    const error = new ExpectedIsNotIncludedInActualStringError(actualString, expectedSubstring)

    expect(error.rawMessage()).toBe(`
      Actual string: %actual%br
      does not includes substring: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ExpectedIsNotIncludedInActualStringError(actualString, expectedSubstring)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(actualString)
    expect(error.arguments().expected).toBe(expectedSubstring)
  })
})
