import { ExpectedIsIncludedInActualStringError } from "../../../src/lib/expectations/errors/ExpectedIsIncludedInActualStringError.js"

describe('ExpectedIsIncludedInActualStringError', () => {
  const actualList = ['aElement']
  const expectedElement = actualList[0]

  it('retrieves the error message', () => {

    const error = new ExpectedIsIncludedInActualStringError(actualList, expectedElement)

    expect(error.rawMessage()).toBe(`
      Actual string: %actual%br
      includes substring: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ExpectedIsIncludedInActualStringError(actualList, expectedElement)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(actualList)
    expect(error.arguments().expected).toBe(expectedElement)
  })
})
