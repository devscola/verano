import { ActualIsTruthyError } from "../../../src/lib/expectations/errors/ActualIsTruthyError.js"

describe('ActualIsTruthyError', () => {
  const actual = 'truthy'

  it('knows if is an Expectation', () => {

    const error = new ActualIsTruthyError(actual)

    expect(error.isExpectationError).toBe(true)
  })

  it('retrieves the error message', () => {

    const error = new ActualIsTruthyError(actual)

    expect(error.rawMessage()).toBe('Expect %actual not to be truthy')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsTruthyError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
