import { ExpectedIsIncludedInActualListError } from "../../../src/lib/expectations/errors/ExpectedIsIncludedInActualListError.js"

describe('ExpectedIsIncludedInActualListError', () => {
  const actualList = ['aElement']
  const expectedElement = actualList[0]

  it('retrieves the error message', () => {

    const error = new ExpectedIsIncludedInActualListError(actualList, expectedElement)

    expect(error.rawMessage()).toBe(`
      Actual list: %actual%br
      includes: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ExpectedIsIncludedInActualListError(actualList, expectedElement)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(actualList)
    expect(error.arguments().expected).toBe(expectedElement)
  })
})
