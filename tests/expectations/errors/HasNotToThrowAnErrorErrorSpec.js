import { HasNotToThrowAnErrorError } from "../../../src/lib/expectations/errors/HasNotToThrowAnErrorError.js"

describe('HasNotToThrowAnErrorError', () => {
  const TestHasNotError = class extends Error {}
  const retrievedError = new TestHasNotError()

  it('retrieves the error message', () => {

    const error = new HasNotToThrowAnErrorError(retrievedError)

    expect(error.rawMessage()).toBe(`
      Expect not to throw an error,%br
      but was thrown: %error
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new HasNotToThrowAnErrorError(retrievedError)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().error).toBe('TestHasNotError')
  })
})
