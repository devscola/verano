import { ActualIsUndefinedError } from "../../../src/lib/expectations/errors/ActualIsUndefinedError.js"

describe('ActualIsUndefinedError', () => {
  const actual = 'something'

  it('retrieves the error message', () => {

    const error = new ActualIsUndefinedError(actual)

    expect(error.rawMessage()).toBe('Expect %actual to be undefined')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsUndefinedError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
