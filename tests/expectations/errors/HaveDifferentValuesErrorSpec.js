import { HaveDifferentValuesError } from "../../../src/lib/expectations/errors/HaveDifferentValuesError.js"

describe('HaveDifferentValuesError', () => {
  const actual = { aDictionary: 'aValue' }
  const expected = { aDictionary: 'anotherValue' }

  it('retrieves the error message', () => {

    const error = new HaveDifferentValuesError(actual, expected)

    expect(error.rawMessage()).toBe(`
      Dictionaries have different values:%br
      Received: %actual%br
      Expected: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new HaveDifferentValuesError(actual, expected)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(order(actual))
    expect(error.arguments().expected).toBe(order(expected))
  })

  function order(dictionary) {
    const ordered = {}
    const keys = Object.keys(dictionary)
    const copyDictionary = {...dictionary}
    const orderedKeys = keys.sort()

    orderedKeys.forEach((key) => {
      ordered[key] = copyDictionary[key]

      delete copyDictionary[key]
    })

    return JSON.stringify(ordered)
  }
})
