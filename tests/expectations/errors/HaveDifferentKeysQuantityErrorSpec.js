import { HaveDifferentKeysQuantityError } from "../../../src/lib/expectations/errors/HaveDifferentKeysQuantityError.js"

describe('HaveDifferentKeysQuantityError', () => {
  const actual = { aDictionary: 'value' }
  const expected = { anotherDictionary: 'value', oneMoreKey: 'value' }

  it('retrieves the error message', () => {

    const error = new HaveDifferentKeysQuantityError(actual, expected)

    expect(error.rawMessage()).toBe(`
      Dictionaries have different quantity of keys:%br
      Received: %actual%br
      Expected: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new HaveDifferentKeysQuantityError(actual, expected)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(order(actual))
    expect(error.arguments().expected).toBe(order(expected))
  })

  function order(dictionary) {
    const ordered = {}
    const keys = Object.keys(dictionary)
    const copyDictionary = {...dictionary}
    const orderedKeys = keys.sort()

    orderedKeys.forEach((key) => {
      ordered[key] = copyDictionary[key]

      delete copyDictionary[key]
    })

    return JSON.stringify(ordered)
  }
})
