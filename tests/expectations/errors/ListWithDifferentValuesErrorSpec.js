import { ListWithDifferentValuesError } from "../../../src/lib/expectations/errors/ListWithDifferentValuesError.js"

describe('ListWithDifferentValuesError', () => {
  const positiveErrorMessage = 'The lists have different values'
  const actual = [1, 3]
  const expected = [2, 1]

  it('retrieves the error message', () => {

    const error = new ListWithDifferentValuesError(actual, expected)

    expect(error.rawMessage()).toBe(`
      %message%br
      Received ordered list: %actual%br
      Expected ordered list: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ListWithDifferentValuesError(actual, expected)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(3)
    expect(error.arguments().actual).toBe(actual)
    expect(error.arguments().expected).toBe(expected)
    expect(error.arguments().message).toBe(positiveErrorMessage)
  })

  it('retrieves the negated message error argument', () => {
    const negated = true

    const error = new ListWithDifferentValuesError(actual, expected, negated)

    expect(error.arguments().message).toBe('The lists are equal')
  })
})
