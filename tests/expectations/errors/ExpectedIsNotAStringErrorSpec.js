import { ExpectedIsNotAStringError } from "../../../src/lib/expectations/errors/ExpectedIsNotAStringError.js"

describe('ExpectedIsNotAStringError', () => {
  const actual = ['thisIsNotAList']

  it('retrieves the error message', () => {

    const error = new ExpectedIsNotAStringError(actual)

    expect(error.rawMessage()).toBe(`
      %actual%br
      is not a string, is a %actualType
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ExpectedIsNotAStringError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(actual)
    expect(error.arguments().actualType).toBe('object')
  })
})
