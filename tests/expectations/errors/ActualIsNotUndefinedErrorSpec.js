import { ActualIsNotUndefinedError } from "../../../src/lib/expectations/errors/ActualIsNotUndefinedError.js"

describe('ActualIsNotUndefinedError', () => {
  const actual = 'something'

  it('retrieves the error message', () => {

    const error = new ActualIsNotUndefinedError(actual)

    expect(error.rawMessage()).toBe('Expect %actual NOT to be undefined')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsNotUndefinedError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
