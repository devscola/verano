import { ActualIsNotFalsyError } from "../../../src/lib/expectations/errors/ActualIsNotFalsyError.js"

describe('ActualIsNotFalsyError', () => {
  const actual = 'falsy'

  it('knows if is an Expectation Error', () => {

    const error = new ActualIsNotFalsyError(actual)

    expect(error.isExpectationError).toBe(true)
  })

  it('retrieves the error message', () => {

    const error = new ActualIsNotFalsyError(actual)

    expect(error.rawMessage()).toBe('Expect %actual to be falsy')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsNotFalsyError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
