import { ExpectedIsNotIncludedInActualListError } from "../../../src/lib/expectations/errors/ExpectedIsNotIncludedInActualListError.js"

describe('ExpectedIsNotIncludedInActualListError', () => {
  const actualList = ['aElement']
  const expectedElement = 'expectedElement'

  it('retrieves the error message', () => {

    const error = new ExpectedIsNotIncludedInActualListError(actualList, expectedElement)

    expect(error.rawMessage()).toBe(`
      Actual list: %actual%br
      does not include: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ExpectedIsNotIncludedInActualListError(actualList, expectedElement)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(actualList)
    expect(error.arguments().expected).toBe(expectedElement)
  })
})
