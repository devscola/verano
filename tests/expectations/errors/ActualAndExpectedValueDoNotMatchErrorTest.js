import { ActualAndExpectedValueDoNotMatchError } from "../../../src/lib/expectations/errors/ActualAndExpectedValueDoNotMatchError.js"

describe('ActualAndExpectedValueDoNotMatchError', () => {
  const positiveErrorMessage = 'To be equal'
  const expected = false
  const actual = true

  it('knows if is an Expectation Error', () => {

    const acty = new ActualAndExpectedValueDoNotMatchError(actual, expected)

    expect(acty.isExpectationError).toBe(true)
  })

  it('retrieves the error message', () => {

    const acty = new ActualAndExpectedValueDoNotMatchError(actual, expected)

    expect(acty.rawMessage()).toBe(`
      %message%br
      Received: %actual%br
      Expected: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const acty = new ActualAndExpectedValueDoNotMatchError(actual, expected)

    const errorArguments = Object.keys(acty.arguments())
    expect(errorArguments.length).toBe(3)
    expect(acty.arguments().actual).toBe(actual)
    expect(acty.arguments().expected).toBe(expected)
    expect(acty.arguments().message).toBe(positiveErrorMessage)
  })

  it('retrieves the positive message error argument', () => {

    const acty = new ActualAndExpectedValueDoNotMatchError(actual, expected)

    expect(acty.arguments().message).toBe(positiveErrorMessage)
  })

  it('retrieves the negated message error argument', () => {
    const negated = true

    const acty = new ActualAndExpectedValueDoNotMatchError(actual, expected, negated)

    expect(acty.arguments().message).toBe('NOT to be equal')
  })
})
