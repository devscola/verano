import { CommonMistakeError } from "../../../src/lib/expectations/errors/CommonMistakeError.js"

describe('CommonMistakeError', () => {

  it('retrieves the error message', () => {

    const error = new CommonMistakeError()

    expect(error.rawMessage()).toBe('You have used the method %tobe, did you mean to use %toBe?')
  })

  it('retrieves the error arguments message', () => {

    const error = new CommonMistakeError()

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().tobe).toBe('tobe')
    expect(error.arguments().toBe).toBe('toBe')
  })
})
