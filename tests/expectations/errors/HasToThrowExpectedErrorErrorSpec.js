import { HasToThrowExpectedErrorError } from "../../../src/lib/expectations/errors/HasToThrowExpectedErrorError.js"

describe('HasToThrowExpectedErrorError', () => {
  const anError = class extends Error {}
  const anotherError = class extends Error {}
  const actual = new anError()
  const expected = anotherError

  it('retrieves the error message', () => {

    const error = new HasToThrowExpectedErrorError(actual, expected)

    expect(error.rawMessage()).toBe('Expect to throw error %expected, but throws error %actual')
  })

  it('retrieves the error arguments message', () => {

    const error = new HasToThrowExpectedErrorError(actual, expected)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(actual.constructor.name)
    expect(error.arguments().expected).toBe(anotherError.name)
  })
})
