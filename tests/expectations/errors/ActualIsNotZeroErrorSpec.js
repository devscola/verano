import { ActualIsNotZeroError } from "../../../src/lib/expectations/errors/ActualIsNotZeroError.js"

describe('ActualIsNotZeroError', () => {
  const actual = 'something'

  it('retrieves the error message', () => {

    const error = new ActualIsNotZeroError(actual)

    expect(error.rawMessage()).toBe('Expect %actual to be Zero')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsNotZeroError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
