import { ActualIsNullError } from "../../../src/lib/expectations/errors/ActualIsNullError.js"

describe('ActualIsNullError', () => {
  const actual = 'something'

  it('retrieves the error message', () => {

    const error = new ActualIsNullError(actual)

    expect(error.rawMessage()).toBe('Expect %actual to be null')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsNullError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
