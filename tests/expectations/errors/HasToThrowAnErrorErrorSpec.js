import { HasToThrowAnErrorError } from "../../../src/lib/expectations/errors/HasToThrowAnErrorError.js"

describe('HasToThrowAnErrorError', () => {
  it('retrieves the error message', () => {

    const error = new HasToThrowAnErrorError()

    expect(error.rawMessage()).toBe('Expect to raise an error, but nothing happens')
  })

  it('retrieves the error arguments message', () => {

    const error = new HasToThrowAnErrorError()

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(0)
  })
})
