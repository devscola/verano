import { DictionariesAreEqualError } from "../../../src/lib/expectations/errors/DictionariesAreEqualError.js"

describe('DictionariesAreEqualError', () => {
  it('retrieves the error message', () => {

    const error = new DictionariesAreEqualError()

    expect(error.rawMessage()).toBe('The dictionaries are equal')
  })

  it('retrieves the error arguments message', () => {

    const error = new DictionariesAreEqualError()

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(0)
  })
})
