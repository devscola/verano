import { OnlyCanUseIncludeMatcherIfActualIsAStringOrListError } from "../../../src/lib/expectations/errors/OnlyCanUseIncludeMatcherIfActualIsAStringOrListError.js"

describe('OnlyCanUseIncludeMatcherIfActualIsAStringOrListError', () => {
  it('retrieves the error message', () => {

    const error = new OnlyCanUseIncludeMatcherIfActualIsAStringOrListError()

    expect(error.rawMessage()).toBe('Only can use the matcher #include when actual value type are strings or lists')
  })

  it('retrieves the error arguments message', () => {

    const error = new OnlyCanUseIncludeMatcherIfActualIsAStringOrListError()

    expect(error.arguments()).toBe({})
  })
})
