import { ExpectedIsNotAListError } from "../../../src/lib/expectations/errors/ExpectedIsNotAListError.js"

describe('ExpectedIsNotAListError', () => {
  const actual = 'thisIsNotAList'

  it('retrieves the error message', () => {

    const error = new ExpectedIsNotAListError(actual)

    expect(error.rawMessage()).toBe(`
      %actual%br
      is not a list, is a %actualType
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ExpectedIsNotAListError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(actual)
    expect(error.arguments().actualType).toBe('string')
  })
})
