import { StackTrace } from "../../../../src/lib/expectations/errors/support/StackTrace.js"

describe('StackTrace', () => {
  const stackTrace = "at Expect.toBeFalsy (file:///C:/Verano/Verano.html:1451:13)\nat file:///C:/Verano/Spec.js:4:18\nat it (file:///C:/Verano/Verano.html:1591:5)\nat file:///C:/Verano/Spec.js:3:3\nat describe (file:///C:/Verano/Verano.html:1579:3)\nat file:///C:/Verano/Spec.js:2:1"

  it('remove all about Verano from stack trace', () => {

    const stackTraceWithoutVerano = StackTrace.removeVeranoFrom(stackTrace)

    const includesVerano = stackTraceWithoutVerano.some(line => line.includes('Verano.html'))
    expect(includesVerano).toBeFalsy()
  })

  it('retrieves an empty stack trace when there is not stack', () => {
    const noStack = undefined
    const emptyStackTrace = []

    const stackTrace = StackTrace.removeVeranoFrom(noStack)

    expect(stackTrace).toBe(emptyStackTrace)
  })
})
