import { HaveDifferentKeysError } from "../../../src/lib/expectations/errors/HaveDifferentKeysError.js"

describe('HaveDifferentKeysError', () => {
  const actual = { aDictionary: 'value' }
  const expected = { anotherDictionary: 'value' }

  it('retrieves the error message', () => {

    const error = new HaveDifferentKeysError(actual, expected)

    expect(error.rawMessage()).toBe(`
      Dictionaries have different keys:%br
      Received: %actual%br
      Expected: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new HaveDifferentKeysError(actual, expected)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(2)
    expect(error.arguments().actual).toBe(order(actual))
    expect(error.arguments().expected).toBe(order(expected))
  })

  function order(dictionary) {
    const ordered = {}
    const keys = Object.keys(dictionary)
    const copyDictionary = {...dictionary}
    const orderedKeys = keys.sort()

    orderedKeys.forEach((key) => {
      ordered[key] = copyDictionary[key]

      delete copyDictionary[key]
    })

    return JSON.stringify(ordered)
  }
})
