import { ActualIsNotTruthyError } from "../../../src/lib/expectations/errors/ActualIsNotTruthyError.js"

describe('ActualIsNotTruthyError', () => {
  const actual = 'truthy'

  it('knows if is an Expectation', () => {

    const error = new ActualIsNotTruthyError(actual)

    expect(error.isExpectationError).toBe(true)
  })

  it('retrieves the error message', () => {

    const error = new ActualIsNotTruthyError(actual)

    expect(error.rawMessage()).toBe('Expect %actual to be truthy')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsNotTruthyError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
