import { HasNotToThrowASpecificErrorError } from "../../../src/lib/expectations/errors/HasNotToThrowASpecificErrorError.js"

describe('HasNotToThrowASpecificErrorError',  () => {
  const TestHasNotSpecificError = class extends Error {}
  const retrievedError = new TestHasNotSpecificError()

  it('retrieves the error message', () => {

    const error = new HasNotToThrowASpecificErrorError(retrievedError)

    expect(error.rawMessage()).toBe(`
      Expect not to throw error %error,%br
      but was thrown.
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new HasNotToThrowASpecificErrorError(retrievedError)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().error).toBe('TestHasNotSpecificError')
  })
})
