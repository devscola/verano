import { ListWithDifferentOrderError } from "../../../src/lib/expectations/errors/ListWithDifferentOrderError.js"

describe('ListWithDifferentOrderError', () => {
  const positiveErrorMessage = 'The lists have different order'
  const actual = [2, 1]
  const expected = [1, 2]

  it('retrieves the error message', () => {

    const error = new ListWithDifferentOrderError(actual, expected)

    expect(error.rawMessage()).toBe(`
      %message%br
      Received list: %actual%br
      Expected list: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ListWithDifferentOrderError(actual, expected)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(3)
    expect(error.arguments().actual).toBe(actual)
    expect(error.arguments().expected).toBe(expected)
    expect(error.arguments().message).toBe(positiveErrorMessage)
  })

  it('retrieves the negated message error argument', () => {
    const negated = true

    const error = new ListWithDifferentOrderError(actual, expected, negated)

    expect(error.arguments().message).toBe('The lists are equal')
    expect(error.rawMessage()).toBe(`
      %message%br
      Received list: %actual%br
      Expected list: %expected%br
    `)
  })
})
