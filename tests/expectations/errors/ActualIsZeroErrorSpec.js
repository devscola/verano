import { ActualIsZeroError } from "../../../src/lib/expectations/errors/ActualIsZeroError.js"

describe('ActualIsZeroError', () => {
  const actual = 'something'

  it('retrieves the error message', () => {

    const error = new ActualIsZeroError(actual)

    expect(error.rawMessage()).toBe('Expect %actual NOT to be Zero')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsZeroError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
