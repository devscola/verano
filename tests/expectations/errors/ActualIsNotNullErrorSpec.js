import { ActualIsNotNullError } from "../../../src/lib/expectations/errors/ActualIsNotNullError.js"

describe('ActualIsNotNullError', () => {
  const actual = 'something'

  it('retrieves the error message', () => {

    const error = new ActualIsNotNullError(actual)

    expect(error.rawMessage()).toBe('Expect %actual not to be null')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsNotNullError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
