import { ListWithDifferentSizesError } from "../../../src/lib/expectations/errors/ListWithDifferentSizesError.js"

describe('ListWithDifferentSizesError', () => {
  const positiveErrorMessage = 'The lists have different sizes'
  const actual = [1]
  const expected = [1, 2]

  it('retrieves the error message', () => {

    const error = new ListWithDifferentSizesError(actual, expected)

    expect(error.rawMessage()).toBe(`
      %message%br
      Received size: %actualSize%br
      Expected size: %expectedSize%br
      %br
      Received list: %actual%br
      Expected list: %expected%br
    `)
  })

  it('retrieves the error arguments message', () => {

    const error = new ListWithDifferentSizesError(actual, expected)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(5)
    expect(error.arguments().actual).toBe(actual)
    expect(error.arguments().actualSize).toBe(actual.length)
    expect(error.arguments().expected).toBe(expected)
    expect(error.arguments().expectedSize).toBe(expected.length)
    expect(error.arguments().message).toBe(positiveErrorMessage)
  })

  it('retrieves the positive message error argument', () => {

    const error = new ListWithDifferentSizesError(actual, expected)

    expect(error.arguments().message).toBe(positiveErrorMessage)
  })

  it('retrieves the negated message error argument', () => {
    const negated = true

    const error = new ListWithDifferentSizesError(actual, expected, negated)

    expect(error.arguments().message).toBe('The lists are equal')
    expect(error.rawMessage()).toBe(`
      %message%br
      Received list: %actual%br
      Expected list: %expected%br
    `)
  })
})
