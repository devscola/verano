import { ActualIsFalsyError } from "../../../src/lib/expectations/errors/ActualIsFalsyError.js"

describe('ActualIsFalsyError', () => {
  const actual = 'truthy'

  it('retrieves the error message', () => {

    const error = new ActualIsFalsyError(actual)

    expect(error.rawMessage()).toBe('Expect %actual not to be falsy')
  })

  it('retrieves the error arguments message', () => {

    const error = new ActualIsFalsyError(actual)

    const errorArguments = Object.keys(error.arguments())
    expect(errorArguments.length).toBe(1)
    expect(error.arguments().actual).toBe(actual)
  })
})
