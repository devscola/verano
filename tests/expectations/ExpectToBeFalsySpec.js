import { ActualIsFalsyError } from "../../src/lib/expectations/errors/ActualIsFalsyError.js"
import { ActualIsNotFalsyError } from "../../src/lib/expectations/errors/ActualIsNotFalsyError.js"
import { ExpectToBeFalsy } from "../../src/lib/expectations/ExpectToBeFalsy.js"

describe('ExpectToBeFalsy', () => {
  const falsyValue = undefined
  const truthyValue = true

  it('expects that the actual value is falsy', () => {
    new ExpectToBeFalsy(falsyValue).toBeFalsy()
  })

  it('raises an error when the actual value is not falsy', () => {

    const expectation = () => { new ExpectToBeFalsy(truthyValue).toBeFalsy() }

    expect(expectation).toThrowAnError(ActualIsNotFalsyError)
  })

  describe('when is negated', () => {
    it('does nothing when the actual is truthy', () => {
      new ExpectToBeFalsy(truthyValue).not.toBeFalsy()
    })

    it('throws an error when the actual is falsy', () => {

      const expectation = () => { new ExpectToBeFalsy(falsyValue).not.toBeFalsy() }

      expect(expectation).toThrowAnError(ActualIsFalsyError)
    })
  })
})
