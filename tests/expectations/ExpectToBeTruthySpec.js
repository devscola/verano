import { ActualIsNotTruthyError } from "../../src/lib/expectations/errors/ActualIsNotTruthyError.js"
import { ActualIsTruthyError } from "../../src/lib/expectations/errors/ActualIsTruthyError.js"
import { ExpectTruth } from "../../src/lib/expectations/ExpectTruth.js"

describe('ExpectTruth', () => {
  const truthyValue = 'truthyValue'
  const falsyValue = ''

  it('expects that the actual is truthy', () => {
    new ExpectTruth(truthyValue).toBeTruthy()
  })

  it('raises an error when the actual is falsy', () => {

    const expectation = () => { new ExpectTruth(falsyValue).toBeTruthy() }

    expect(expectation).toThrowAnError(ActualIsNotTruthyError)
  })

  describe('when is negated', () => {
    it('does not throw an error when actual is not truthy', () => {
      new ExpectTruth(falsyValue).not.toBeTruthy()
    })

    it('throws ane error when actual is truthy', () => {

      const expectation = () => { new ExpectTruth(truthyValue).not.toBeTruthy() }

      expect(expectation).toThrowAnError(ActualIsTruthyError)
    })
  })
})
