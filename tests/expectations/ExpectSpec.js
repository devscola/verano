import { ActualAndExpectedValueDoNotMatchError } from "../../src/lib/expectations/errors/ActualAndExpectedValueDoNotMatchError.js"
import { ActualIsFalsyError } from "../../src/lib/expectations/errors/ActualIsFalsyError.js"
import { ActualIsNotFalsyError } from "../../src/lib/expectations/errors/ActualIsNotFalsyError.js"
import { ActualIsNotNullError } from "../../src/lib/expectations/errors/ActualIsNotNullError.js"
import { ActualIsNotTruthyError } from "../../src/lib/expectations/errors/ActualIsNotTruthyError.js"
import { ActualIsNotUndefinedError } from "../../src/lib/expectations/errors/ActualIsNotUndefinedError.js"
import { ActualIsNotZeroError } from "../../src/lib/expectations/errors/ActualIsNotZeroError.js"
import { ActualIsNullError } from "../../src/lib/expectations/errors/ActualIsNullError.js"
import { ActualIsTruthyError } from "../../src/lib/expectations/errors/ActualIsTruthyError.js"
import { ActualIsZeroError } from "../../src/lib/expectations/errors/ActualIsZeroError.js"
import { CommonMistakeError } from "../../src/lib/expectations/errors/CommonMistakeError.js"
import { ExpectedIsIncludedInActualListError } from "../../src/lib/expectations/errors/ExpectedIsIncludedInActualListError.js"
import { ExpectedIsIncludedInActualStringError } from "../../src/lib/expectations/errors/ExpectedIsIncludedInActualStringError.js"
import { ExpectedIsNotIncludedInActualListError } from "../../src/lib/expectations/errors/ExpectedIsNotIncludedInActualListError.js"
import { ExpectedIsNotIncludedInActualStringError } from "../../src/lib/expectations/errors/ExpectedIsNotIncludedInActualStringError.js"
import { HasToThrowAnErrorError } from "../../src/lib/expectations/errors/HasToThrowAnErrorError.js"
import { HaveDifferentValuesError } from "../../src/lib/expectations/errors/HaveDifferentValuesError.js"
import { ListWithDifferentOrderError } from "../../src/lib/expectations/errors/ListWithDifferentOrderError.js"
import { ListWithDifferentSizesError } from "../../src/lib/expectations/errors/ListWithDifferentSizesError.js"
import { ListWithDifferentValuesError } from "../../src/lib/expectations/errors/ListWithDifferentValuesError.js"
import { OnlyCanUseIncludeMatcherIfActualIsAStringOrListError } from "../../src/lib/expectations/errors/OnlyCanUseIncludeMatcherIfActualIsAStringOrListError.js"

describe('Expect', () => {
  it('does not throw an error when two list are equal', () => {
    const someList = ['someValue']
    const theSameList = ['someValue']

    expect(someList).toBe(theSameList)
  })

  it('compares like a list only if values are lists', () => {
    const someValue = 'someValue'
    const longList = ['someValue', 'someValue']
    let hasThrowAnError = false

    try {
      expect(someValue).toBe(longList)
    } catch (error) {
      if (error instanceof ActualAndExpectedValueDoNotMatchError) {
        hasThrowAnError = true
      }
    }

    expect(hasThrowAnError).toBeTruthy()
  })

  it('throw an error when two list have different lengths', () => {
    const shortList = ['someValue']
    const longList = ['someValue', 'someValue']
    let hasThrowAnError = false

    try {
      expect(shortList).toBe(longList)
    } catch (error) {
      if (error instanceof ListWithDifferentSizesError) {
        hasThrowAnError = true
      }
    }

    expect(hasThrowAnError).toBeTruthy()
  })

  it('throw an error when two list have different values', () => {
    const someList = ['someValue', 'someValue']
    const anotherDifferentList = ['someValue', 'anotherValue']
    let hasThrowAnError = false

    try {
      expect(someList).toBe(anotherDifferentList)
    } catch (error) {
      if (error instanceof ListWithDifferentValuesError) {
        hasThrowAnError = true
      }
    }

    expect(hasThrowAnError).toBeTruthy()
  })

  it('throw an error when two list have different orders', () => {
    const someList = [0, 1]
    const anotherListWithDifferentOrder = [1, 0]
    let hasThrowAnError = false

    try {
      expect(someList).toBe(anotherListWithDifferentOrder)
    } catch (error) {
      if (error instanceof ListWithDifferentOrderError) {
        hasThrowAnError = true
      }
    }

    expect(hasThrowAnError).toBeTruthy()
  })

  it('does not raises an error when is negated and the lists are different', () => {
    let hasThrowAnError = false

    try {
      expect([0]).not.toBe([1, 0])
    } catch (error) {
      if (error instanceof ListWithDifferentSizesError) {
        hasThrowAnError = true
      }
    }
    try {
      expect([0, 1]).not.toBe([2, 0, 1])
    } catch (error) {
      if (error instanceof ListWithDifferentValuesError) {
        hasThrowAnError = true
      }
    }

    expect(hasThrowAnError).toBeFalsy()
  })

  it('throws an error when actual is not truthy but is expected', () => {

    const expectation = () => { expect(false).toBeTruthy() }

    expect(expectation).toThrowAnError(ActualIsNotTruthyError)
  })

  it('throws an error when actual is truthy but is not expected', () => {

    const expectation = () => { expect(true).not.toBeTruthy() }

    expect(expectation).toThrowAnError(ActualIsTruthyError)
  })

  it('expects that the actual is null', () => {
    let hasThrowAnError = false

    try {
      expect('notNullValue').toBeNull()
    } catch (error) {
      if (error instanceof ActualIsNullError) {
        hasThrowAnError = true
      } else {
        throw error
      }
    }

    expect(hasThrowAnError).toBeTruthy()
  })

  it('expects that the actual not is null', () => {
    let hasThrowAnError = false

    try {
      expect(null).not.toBeNull()
    } catch (error) {
      if (error instanceof ActualIsNotNullError) {
        hasThrowAnError = true
      } else {
        throw error
      }
    }

    expect(hasThrowAnError).toBeTruthy()
  })

  it('throws an error when expects the act throws an error and does not do it', () => {
    let hasThrowAnError = false
    const actWithoutError = () => { return true }

    try {
      expect(actWithoutError).toThrowAnError()
    } catch (error) {
      if (error instanceof HasToThrowAnErrorError) {
        hasThrowAnError = true
      } else {
        throw error
      }
    }

    expect(hasThrowAnError).toBeTruthy()
  })

  it('does not throw an error when the act does not throw an error and does not expected it', () => {
    const actWithoutError = () => { return true }

    expect(actWithoutError).not.toThrowAnError()
  })

  it('throws an error when expect and actual dictionaries are different', () => {
    let hasThrowAnError = false

    try {
      expect({a: 2}).toBe({a: 3})
    } catch (error) {
      if (error instanceof HaveDifferentValuesError) {
        hasThrowAnError = true
      } else {
        throw error
      }
    }

    expect(hasThrowAnError).toBeTruthy()
  })

  it('does not throw an error when expect and actual dictionaries are different', () => {
    expect({a: 2}).not.toBe({b: 3})
  })

  it('expects that actual has to be Zero', () => {

    const isZeroExpectation = () => { expect(1).toBeZero() }
    const isNotZeroExpectation = () => { expect(0).not.toBeZero() }

    expect(isZeroExpectation).toThrowAnError(ActualIsNotZeroError)
    expect(isNotZeroExpectation).toThrowAnError(ActualIsZeroError)
  })

  it('throws an error when the actual is not undefined', () => {

    const expectation = () => { expect('notUndefinedValue').toBeUndefined() }

    expect(expectation).toThrowAnError(ActualIsNotUndefinedError)
  })

  it('does not throw an error when the actual is not undefined', () => {
    expect('notUndefinedValue').not.toBeUndefined()
  })

  it('does not throw an error when the actual is undefined', () => {
    expect(undefined).toBeUndefined()
  })

  it('throws an error when a list does nos includes an element', () => {

    const expectation = () => { expect([0,1,2]).include(3) }

    expect(expectation).toThrowAnError(ExpectedIsNotIncludedInActualListError)
  })

  it('throws an error when expect to not include an element and do it', () => {

    const expectation = () => { expect([0,1,2]).not.include(0) }

    expect(expectation).toThrowAnError(ExpectedIsIncludedInActualListError)
  })

  it('throws an error when actual not is falsy but is expected', () => {

    const expectation = () => { expect(true).toBeFalsy() }

    expect(expectation).toThrowAnError(ActualIsNotFalsyError)
  })

  it('throws an error when actual is falsy but is not expected', () => {

    const expectation = () => { expect(false).not.toBeFalsy() }

    expect(expectation).toThrowAnError(ActualIsFalsyError)
  })

  it('throws an error when actual string not includes substring but is expected', () => {

    const expectation = () => { expect('string').include('notIncluded') }

    expect(expectation).toThrowAnError(ExpectedIsNotIncludedInActualStringError)
  })

  it('throws an error when actual string not includes substring but is not expected', () => {

    const expectation = () => { expect('string').not.include('str') }

    expect(expectation).toThrowAnError(ExpectedIsIncludedInActualStringError)
  })

  it('throws an error when check if actual includes expected, but actual is not a list or string', () => {

    const stringExpectation = () => { expect({ not: 'string' }).not.include('substring') }
    const listExpectation = () => { expect({ not: 'list' }).not.include('element') }

    expect(stringExpectation).toThrowAnError(OnlyCanUseIncludeMatcherIfActualIsAStringOrListError)
    expect(listExpectation).toThrowAnError(OnlyCanUseIncludeMatcherIfActualIsAStringOrListError)
  })

  it('throws an common mistake error when some use tobe matcher', () => {

    const commonMistakeError = () => { expect('anything').tobe('whatever') }

    expect(commonMistakeError).toThrowAnError(CommonMistakeError)
  })
})
