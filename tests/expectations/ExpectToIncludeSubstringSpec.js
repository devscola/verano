import { ExpectedIsIncludedInActualStringError } from "../../src/lib/expectations/errors/ExpectedIsIncludedInActualStringError.js"
import { ExpectedIsNotAStringError } from "../../src/lib/expectations/errors/ExpectedIsNotAStringError.js"
import { ExpectedIsNotIncludedInActualStringError } from "../../src/lib/expectations/errors/ExpectedIsNotIncludedInActualStringError.js"
import { ExpectToIncludeSubstring } from "../../src/lib/expectations/ExpectToIncludeSubstring.js"

describe('ExpectToIncludeSubstring', () => {
  const actualString = 'A String with things'
  const substringInActual = ' String '
  const notIncludedSubstring = 'notIncludedSubstring'

  it('does not throw an error when the expected is at actual string', () => {

    const expectation = () => {
      new ExpectToIncludeSubstring(actualString).include(substringInActual)
    }

    expect(expectation).not.toThrowAnError()
  })

  it('throws an error when the expected is not included in the actual string', () => {

    const expectation = () => {
      new ExpectToIncludeSubstring(actualString).include(notIncludedSubstring)
    }

    expect(expectation).toThrowAnError(ExpectedIsNotIncludedInActualStringError)
  })

  it('throws an error when the actual is not a string', () => {

    const expectation = () => { new ExpectToIncludeSubstring(['1notAList']) }

    expect(expectation).toThrowAnError(ExpectedIsNotAStringError)
  })

  describe('when not has to include expected substring', () => {
    it('does not throw an error when actual does not include expected substring', () => {

      const expectation = () => {
        new ExpectToIncludeSubstring(actualString).not.include(notIncludedSubstring)
      }

      expect(expectation).not.toThrowAnError()
    })

    it('throws an error when actual includes expected element', () => {

      const expectation = () => {
        new ExpectToIncludeSubstring(actualString).not.include(substringInActual)
      }

      expect(expectation).toThrowAnError(ExpectedIsIncludedInActualStringError)
    })
  })
})
