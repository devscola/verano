import { HasNotToThrowAnErrorError } from "../../src/lib/expectations/errors/HasNotToThrowAnErrorError.js"
import { HasNotToThrowASpecificErrorError } from "../../src/lib/expectations/errors/HasNotToThrowASpecificErrorError.js"
import { HasToThrowAnErrorError } from "../../src/lib/expectations/errors/HasToThrowAnErrorError.js"
import { HasToThrowExpectedErrorError } from "../../src/lib/expectations/errors/HasToThrowExpectedErrorError.js"
import { ExpectAnError } from "../../src/lib/expectations/ExpectAnError.js"

describe('Expect to throw an error', () => {
  const SpectedError = class extends Error {}
  const TestError = class extends Error {}
  const actWithError = () => { throw new TestError() }
  const actWithoutError = () => {}

  it('does not fail when the expectation throws an error', () => {
    new ExpectAnError(actWithError).toThrowAnError()
  })

  it('fails when the expectation does not throw an error', () => {
    let errorWasThrowed = false

    try {
      new ExpectAnError(actWithoutError).toThrowAnError()
    } catch (error) {
      if (error instanceof HasToThrowAnErrorError) {
        errorWasThrowed = true
      }
    }

    expect(errorWasThrowed).toBeTruthy()
  })

  it('fails when the expectation does not throw an specific error', () => {
    let errorWasThrowed = false

    try {
      new ExpectAnError(actWithError).toThrowAnError(SpectedError)
    } catch (error) {
      if (error instanceof HasToThrowExpectedErrorError) {
        errorWasThrowed = true
      }
    }

    expect(errorWasThrowed).toBeTruthy()
  })

  describe('when does not expect to throw an error', () => {
    it('does not fail when the expectation does not throw an error', () => {

      new ExpectAnError(actWithoutError).not.toThrowAnError()
    })

    it('fails when the expectation throw an error', () => {
      let errorWasThrowed = false

      try {
        new ExpectAnError(actWithError).not.toThrowAnError()
      } catch (error) {
        if (error instanceof HasNotToThrowAnErrorError) {
          errorWasThrowed = true
        }
      }

      expect(errorWasThrowed).toBeTruthy()
    })

    it('fails when the expectation throw a specific error', () => {
      let errorWasThrowed = false

      try {
        new ExpectAnError(actWithError).not.toThrowAnError(TestError)
      } catch (error) {
        if (error instanceof HasNotToThrowASpecificErrorError) {
          errorWasThrowed = true
        }
      }

      expect(errorWasThrowed).toBeTruthy()
    })
  })
})
