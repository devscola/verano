import { ActualIsNotNullError } from "../../src/lib/expectations/errors/ActualIsNotNullError.js"
import { ActualIsNullError } from "../../src/lib/expectations/errors/ActualIsNullError.js"
import { ExpectNull } from "../../src/lib/expectations/ExpectNull.js"

describe('ExpectToBeNull', () => {
  it('does not raise an error when the actual value is null', () => {
    const actualNullValue = null

    new ExpectNull(actualNullValue).toBeNull()
  })

  it('raises an error when the actual value is not null', () => {
    let hasRaisedAnError = false

    try {
      new ExpectNull('notActualNullValue').toBeNull()
    } catch (error) {
      if (error instanceof ActualIsNullError) {
        hasRaisedAnError = true
      } else {
        throw error
      }
    }

    expect(hasRaisedAnError).toBeTruthy()
  })

  describe('when is negated', () => {
    it('does not raise an error when a non null value has to not be null', () => {
      new ExpectNull('notActualNullValue').not.toBeNull()
    })

    it('raises an error when a null value has to not be null', () => {
      const actualNullValue = null
      let hasRaisedAnError = false

      try {
        new ExpectNull(actualNullValue).not.toBeNull()
      } catch (error) {
        if (error instanceof ActualIsNotNullError) {
          hasRaisedAnError = true
        } else {
          throw error
        }
      }

      expect(hasRaisedAnError).toBeTruthy()
    })
  })
})
