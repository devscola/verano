import { DictionariesAreEqualError } from "../../src/lib/expectations/errors/DictionariesAreEqualError.js"
import { HaveDifferentKeysError } from "../../src/lib/expectations/errors/HaveDifferentKeysError.js"
import { HaveDifferentKeysQuantityError } from "../../src/lib/expectations/errors/HaveDifferentKeysQuantityError.js"
import { HaveDifferentValuesError } from "../../src/lib/expectations/errors/HaveDifferentValuesError.js"
import { ExpectDictionary } from "../../src/lib/expectations/ExpectDictionary.js"

describe('ExpectDictionary', () => {
  const theDictionary = { actual: 'expected' }
  const theDifferentDictionary = { different: 'different', dictionary: 'dictionary' }

  it('does not throw an error when actual and expected are equal', () => {

    const expectation = () => { new ExpectDictionary(theDictionary).toBe(theDictionary) }

    expect(expectation).not.toThrowAnError()
  })

  it('throws the error when actual and expected have different quantity keys', () => {

    const expectation = () => { new ExpectDictionary(theDictionary).toBe(theDifferentDictionary) }

    expect(expectation).toThrowAnError(HaveDifferentKeysQuantityError)
  })

  it('throws the error when actual and expected have different keys', () => {
    const dictionaryWithDifferentKey = { expected: 'actual' }

    const expectation = () => { new ExpectDictionary(theDictionary).toBe(dictionaryWithDifferentKey) }

    expect(expectation).toThrowAnError(HaveDifferentKeysError)
  })

  it('throws the error when actual and expected have different values', () => {
    const dictionaryWithDifferentValues = { actual: 'anotherExpected', expected: 'anotherActual' }
    const anotherDictionary = { expected: 'anotherExpected', actual: 'anotherActual' }

    const expectation = () => { new ExpectDictionary(anotherDictionary).toBe(dictionaryWithDifferentValues) }

    expect(expectation).toThrowAnError(HaveDifferentValuesError)
  })

  describe('dictionaries are negated', () => {
    it('does not throw an error when actual and expected are different', () => {

      const expectation = () => { new ExpectDictionary(theDictionary).not.toBe(theDifferentDictionary) }

      expect(expectation).not.toThrowAnError()
    })

    it('throws an error when actual and expected are equal', () => {

      const expectation = () => { new ExpectDictionary(theDictionary).not.toBe(theDictionary) }

      expect(expectation).toThrowAnError(DictionariesAreEqualError)
    })
  })
})
