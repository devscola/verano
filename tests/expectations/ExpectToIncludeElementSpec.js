import { ExpectedIsIncludedInActualListError } from "../../src/lib/expectations/errors/ExpectedIsIncludedInActualListError.js"
import { ExpectedIsNotAListError } from "../../src/lib/expectations/errors/ExpectedIsNotAListError.js"
import { ExpectedIsNotIncludedInActualListError } from "../../src/lib/expectations/errors/ExpectedIsNotIncludedInActualListError.js"
import { ExpectToIncludeElement } from "../../src/lib/expectations/ExpectToIncludeElement.js"

describe('ExpectToIncludeElement', () => {
  const actualList = [1,2,2,3,4]
  const elementAtList = actualList[0]
  const notIncludedElement = 'notIncludedElement'

  it('does not throw an error when the expected is at actual list', () => {

    const expectation = () => {
      new ExpectToIncludeElement(actualList).include(elementAtList)
    }

    expect(expectation).not.toThrowAnError()
  })

  it('throws an error when the expected is not included in the actual list', () => {

    const expectation = () => {
      new ExpectToIncludeElement(actualList).include(notIncludedElement)
    }

    expect(expectation).toThrowAnError(ExpectedIsNotIncludedInActualListError)
  })

  it('throws an error when the actual is not a list', () => {

    const expectation = () => { new ExpectToIncludeElement('notAList') }

    expect(expectation).toThrowAnError(ExpectedIsNotAListError)
  })

  describe('when not has to include expected element', () => {
    it('does not throw an error when actual does not include expected element', () => {

      const expectation = () => {
        new ExpectToIncludeElement(actualList).not.include(notIncludedElement)
      }

      expect(expectation).not.toThrowAnError()
    })

    it('throws an error when actual includes expected element', () => {

      const expectation = () => {
        new ExpectToIncludeElement(actualList).not.include(elementAtList)
      }

      expect(expectation).toThrowAnError(ExpectedIsIncludedInActualListError)
    })
  })
})
