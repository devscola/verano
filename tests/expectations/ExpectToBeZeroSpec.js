import { ActualIsNotZeroError } from "../../src/lib/expectations/errors/ActualIsNotZeroError.js"
import { ActualIsZeroError } from "../../src/lib/expectations/errors/ActualIsZeroError.js"
import { ExpectToBeZero } from "../../src/lib/expectations/ExpectToBeZero.js"

describe('ExpectToBeZero', () => {
  const nonZeroValue = 12

  it('expects that actual is Zero', () => {
    const actual = 0

    const expectation = () => { new ExpectToBeZero(actual).toBeZero() }

    expect(expectation).not.toThrowAnError()
  })

  it('raises an error when expects that actual is not Zero', () => {

    const expectation = () => { new ExpectToBeZero(nonZeroValue).toBeZero() }

    expect(expectation).toThrowAnError(ActualIsNotZeroError)
  })

  describe('is negated', () => {
    it('expects that actual is not Zero', () => {

      const expectation = () => { new ExpectToBeZero(nonZeroValue).not.toBeZero() }

      expect(expectation).not.toThrowAnError()
    })

    it('raises an error when that actual is Zero', () => {

      const expectation = () => { new ExpectToBeZero(0).not.toBeZero() }

      expect(expectation).toThrowAnError(ActualIsZeroError)
    })
  })
})
