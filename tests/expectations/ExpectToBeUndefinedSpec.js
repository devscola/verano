import { ActualIsNotUndefinedError } from "../../src/lib/expectations/errors/ActualIsNotUndefinedError.js"
import { ActualIsUndefinedError } from "../../src/lib/expectations/errors/ActualIsUndefinedError.js"
import { ExpectToBeUndefined } from "../../src/lib/expectations/ExpectToBeUndefined.js"

describe('ExpectToBeUndefined', () => {
  it('does not throw an error when the actual is undefined', () => {
    const actual = undefined

    const expectation = () => { new ExpectToBeUndefined(actual).toBeUndefined() }

    expect(expectation).not.toThrowAnError()
  })

  it('throws an error when the actual is not undefined', () => {
    const actual = 'notUndefinedValue'

    const expectation = () => { new ExpectToBeUndefined(actual).toBeUndefined() }

    expect(expectation).toThrowAnError(ActualIsNotUndefinedError)
  })

  describe('when not to be undefined', () => {
    it('does not throw an error when the actual not is undefined', () => {
      const actual = 'notUndefinedValue'

      const expectation = () => { new ExpectToBeUndefined(actual).not.toBeUndefined() }

      expect(expectation).not.toThrowAnError()
    })

    it('throws an error when the actual is not undefined', () => {
      const actual = undefined

      const expectation = () => { new ExpectToBeUndefined(actual).not.toBeUndefined() }

      expect(expectation).toThrowAnError(ActualIsUndefinedError)
    })
  })
})
