import { Drawer } from "../src/lib/presentation/Drawer.js"
import { normalize } from "../src/lib/Verano.js"
import { DrawerSpy } from "./support/DrawerSpy.js"

function expectIncludes(error, expected, actual){
  const notIncludesExpected = error.arguments().expected !== expected
  const notIncludesActual = error.arguments().actual !== actual

  if (notIncludesExpected || notIncludesActual) {
    throw error
  }
}

let itExpect
const testDrawer = new Drawer(document)

testDrawer.subjectUnderTest('Expect')
itExpect = 'does nothing when the actual value and expected value are the same'
try {
  expect(true).toBe(true)
  testDrawer.testPassing(itExpect)
  passingTestCount ++
} catch (error) {
  testDrawer.testFailing(itExpect, normalize(error))
}

itExpect = 'throws an error when actual and expected value don\'t match'
try {
  expect(false).toBe(true)
  throw new Error()
} catch (error) {
  if (!error.isExpectationError) {
    testDrawer.testFailing(itExpect, normalize(error))
  }
  testDrawer.testPassing(itExpect)
  passingTestCount ++
}

itExpect = 'when fails shows the expected and actual values'
const expected = 'expected value'
const actual = 'actual value'
try {
  expect(actual).toBe(expected)
  throw new Error()
} catch (error) {
  try {
    expectIncludes(error, expected, actual)
    testDrawer.testPassing(itExpect)
    passingTestCount ++
  } catch (error) {
    testDrawer.testFailing(itExpect, normalize(error))
  }
}

itExpect = 'does nothing when the condition is negated and the expected and actual doesn\'t match'
try {
  expect(false).not.toBe(true)
  testDrawer.testPassing(itExpect)
  passingTestCount ++
} catch (error) {
  testDrawer.testFailing(itExpect, normalize(error))
}

itExpect = 'throws an error when the condition is negated and the expected and actual match'
try {
  expect(true).not.toBe(true)
  throw new Error()
} catch (error) {
  if (error.isExpectationError) {
    testDrawer.testPassing(itExpect)
    passingTestCount ++
  } else {
    testDrawer.testFailing(itExpect, normalize(error))
  }
}

itExpect = 'knows compare nulls value'
try {
  expect(null).toBe(null)
} catch (error) {
  if (error.isExpectationError) {
    testDrawer.testPassing(itExpect)
    passingTestCount ++
  } else {
    testDrawer.testFailing(itExpect, normalize(error))
  }
}

itExpect = 'it'
testDrawer.subjectUnderTest(itExpect)
let testWasExecuted = false
drawer = new DrawerSpy()
try {
  it('executes a test', () => {testWasExecuted = true})
  passingTestCount ++
  expect(testWasExecuted).toBe(true)
  expect(drawer.hasCountPassingTest()).toBe(true)
} catch (error) {
  testDrawer.testFailing(itExpect, normalize(error))
}
drawer.disableSpy()

it('', () => {
  let haveBeenRaisedAnError = false

  try {
    it('does not raises an error if does not receive a test')
  } catch (error) {
    haveBeenRaisedAnError = true
  }

  expect(haveBeenRaisedAnError).toBe(false)
})

testDrawer.subjectUnderTest('describe')
it('executes a test suit', () => {
  let testSuitWasExecuted = false

  describe('', () => {testSuitWasExecuted = true})

  expect(testSuitWasExecuted).toBe(true)
})

it('does not raise an error if does not receive a test suit', () => {
  let haveBeenRaisedAnError = false

  try {
    describe('')
  } catch (error) {
    haveBeenRaisedAnError = true
  }

  expect(haveBeenRaisedAnError).toBe(false)
})

it('counts the passing tests', () => {
  expect(passingTestCount).not.toBe(0)
})

it('does not throw a common errors', () => {
  drawer = new DrawerSpy()

  const act = () => {
    describe('describe does not throw a common errors', () => {
      throw 'CommonError'
    })
  }

  expect(act).not.toThrowAnError()
  drawer.disableSpy()
})

it('draws an error happens in this describe when an error is thrown in its block', () => {
  drawer = new DrawerSpy()

  describe('', () => {
    throw 'CommonError'
  })

  expect(drawer.hasDraw('An error is thrown in this describe')).toBe(true)
  drawer.disableSpy()
})
