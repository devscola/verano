import { DrawerSpy } from "./support/DrawerSpy.js"

describe('xit', () => {
  beforeAll(() => {
    drawer = new DrawerSpy()
  })

  afterAll(() => {
    drawer.disableSpy()
  })

  it('does not executes the test', () => {
    let hasBeenExecuted = false

    xit('testName', () => { hasBeenExecuted = true })

    expect(hasBeenExecuted).toBe(false)
  })

  it('draws description as skip', () => {
    const descriptionTest = 'someDescription'

    xit(descriptionTest)

    expect(drawer.hasDraw(descriptionTest)).toBe(true)
  })
})
