
let hasBeenExecutedAfterEach = false

describe('afterEach', () => {
  afterEach(() => {
    hasBeenExecutedAfterEach = true
  })

  it('does not executes before a test', () => {
    expect(hasBeenExecutedAfterEach).toBe(false)
  })

  it('executes after a test', () => {
    expect(hasBeenExecutedAfterEach).toBe(true)
  })
})

describe('afterEachHook', () => {
  it('is cleaned after test suit', () => {
    hasBeenExecutedAfterEach = false

    afterEachHooks.clean(currentTestSuit)

    expect(hasBeenExecutedAfterEach).toBe(false)
  })
})
