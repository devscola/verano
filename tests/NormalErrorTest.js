import { NormalError } from "../src/lib/NormalError.js"

describe('NormalError', () => {
  it('has not argument errors', () => {

    const argumentsError = new NormalError(error()).arguments()

    const errorArguments = Object.keys(argumentsError)
    expect(errorArguments.length).toBe(0)
  })

  it('has an error message', () => {

    const errorMessage = new NormalError(error()).rawMessage()

    expect(errorMessage).toBe(errorAsString())
  })

  it('has a stack trace', () => {
    const commonError = error()
    const commonErrorStackTrace = commonError.stack.split('\n')

    const stackTrace = new NormalError(commonError).stackTrace()

    expect(stackTrace).toBe(commonErrorStackTrace)
    expect(stackTrace.length).toBe(7)
  })

  it('has a stack trace even for a throwed string', () => {

    const stackTrace = new NormalError('throwedString').stackTrace()

    expect(stackTrace.length).toBe(6)
    expect(stackTrace[0]).toBe("Error")
  })

  function error() {
    try {
      throw new Error()
    } catch(error) {
      return error
    }
  }

  function errorAsString() {
    return error().toString()
  }
})
