
let hasBeenExecutedAfterAll = false

describe('afterAll', () => {
  afterAll(() => {
    hasBeenExecutedAfterAll = true
  })

  it('does not executes at the beginning of the current suit', () => {
    expect(hasBeenExecutedAfterAll).toBe(false)
  })
})

it('executes at the ending of the current suit', () => {
  expect(hasBeenExecutedAfterAll).toBe(true)
})

describe('afterAllHook', () => {
  it('is cleaned after test suit', () => {
    hasBeenExecutedAfterAll = false

    afterAllHooks.clean(testSuitsRunning)

    expect(hasBeenExecutedAfterAll).toBe(false)
  })
})
