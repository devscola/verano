
export class DocumentSpy {
  constructor() {
    this.innerHTML = ''
  }

  querySelector() {
    return this
  }

  html() {
    return this.innerHTML
  }

  includes(text) {
    return this.innerHTML.includes(text)
  }
}
