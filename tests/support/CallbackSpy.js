
export class CallbackSpy {
  constructor() {
    this.isExecuted = false
  }

  execute = () => {
    this.isExecuted = true
  }

  hasBeenExecuted() {
    return this.isExecuted
  }
}
