import { Drawer } from "../../src/lib/presentation/Drawer.js"

export class DrawerSpy extends Drawer {
  constructor() {
    super(document)

    this.text = null
    this.countedPassingTest = false
  }

  skippedTest(description) {
    this.text = description
  }

  testFailing(text, _) {
    this.text = text
  }

  passingTestCount(_) {
    this.countedPassingTest = true
  }

  skippingTestCount(_) {

  }

  disableSpy() {
    drawer = new Drawer(document)
  }

  hasDraw(text) {
    return this.text.includes(text)
  }

  hasCountPassingTest() {
    return this.countedPassingTest
  }
}
