import { ActualAndExpectedValueDoNotMatchError } from "../../src/lib/expectations/errors/ActualAndExpectedValueDoNotMatchError.js"
import { ListWithDifferentSizesError } from "../../src/lib/expectations/errors/ListWithDifferentSizesError.js"
import { NormalError } from "../../src/lib/NormalError.js"
import { ErrorPresenter } from "../../src/lib/presentation/ErrorPresenter.js"

describe('ErrorPresenter', () => {
  const testDescription = 'someText'

  it('presents Acty as HTML', () => {
    const actual = true
    const expected = false
    const error = new ActualAndExpectedValueDoNotMatchError(actual, expected)

    const html = ErrorPresenter.with(testDescription, error).asHtml()

    expect(html.includes(`<h3 class="test fails js-test-failing">${testDescription}</h3>`)).toBe(true)
    expect(html.includes(`<div class="error-message">`)).toBe(true)
    expect(html.includes(`<strong>To be equal</strong>`)).toBe(true)
    expect(html.includes(`Received: <strong>${actual}</strong><br />`)).toBe(true)
    expect(html.includes(`Expected: <strong>${expected}</strong><br />`)).toBe(true)
    expect(html.includes('<p class="error-trace">')).toBe(true)
    expect(html.includes('tests/presentation/ErrorPresenterTest.js')).toBe(true)
  })

  it('presents NormalError as HTML', () => {
    const error = new NormalError(systemError())

    const html = ErrorPresenter.with(testDescription, error).asHtml()

    expect(html.includes(`<div class="error-message"><div class="expected-message">ReferenceError: undefinedFunction is not defined`)).toBe(true)
    expect(html.includes(`<p class="error-trace">`)).toBe(true)
    expect(html.includes(`systemError`)).toBe(true)
  })

  it('presents string error arguments in quotes marks', () => {
    const error = new ActualAndExpectedValueDoNotMatchError(' 2 ', 2)

    const html = ErrorPresenter.with(testDescription, error).asHtml()

    expect(html.includes('<strong><pre>" 2 "</pre></strong>')).toBe(true)
    expect(html.includes(`<strong>2</strong>`)).toBe(true)
  })

  it('presents lists error arguments like a list', () => {
    const error = new ListWithDifferentSizesError([1,"2"], [1,"2",'3'])

    const html = ErrorPresenter.with(testDescription, error).asHtml()

    expect(html.includes(`<strong>[1,"2"]</strong>`)).toBe(true)
    expect(html.includes('<strong>[1,"2","3"]</strong>')).toBe(true)
  })

  function systemError() {
    try {
      undefinedFunction()
    } catch (error) {
      return error
    }
  }
})
