import { ActualAndExpectedValueDoNotMatchError } from "../../src/lib/expectations/errors/ActualAndExpectedValueDoNotMatchError.js"
import { Drawer } from "../../src/lib/presentation/Drawer.js"
import { DocumentSpy } from "../support/DocumentSpy.js"

describe('Drawer', () => {
  const testDescription = 'Text description for document'
  let documentSpy

  beforeEach(() => {
    documentSpy = new DocumentSpy()
  })

  it('draws the subject under test in the document', () => {
    const subjectUnderTest = 'Subject under test'
    const emoji = "&#128218;"

    new Drawer(documentSpy).subjectUnderTest(subjectUnderTest)

    expect(documentSpy.includes(`<h1 class="suite-description">${emoji} ${subjectUnderTest}</h1>`)).toBe(true)
  })

  it('draws a green test description in the document', () => {

    new Drawer(documentSpy).testPassing(testDescription)

    expect(documentSpy.includes(`<h3 class="test passes">${testDescription}</h3>`)).toBe(true)
  })

  it('draws a red test description and its error in the document', () => {
    const emoji = "&#x274C;"
    new Drawer(documentSpy).testFailing(testDescription, error())

    expect(documentSpy.includes(`<h3 class="test fails js-test-failing">${testDescription} ${emoji}</h3>`)).toBe(true)
  })

  it('draws a grey test description in the document' , () => {
    const emoji = "&#x1F634;"
    new Drawer(documentSpy).skippedTest(testDescription, error())

    expect(documentSpy.includes(`<h3 class="test skip">${testDescription} ${emoji}</h3>`)).toBe(true)
  })

  it('draws beforeAll errors' , () => {
    const emoji = "&#x274C;"

    new Drawer(documentSpy).errorInBeforeAll(error())

    expect(documentSpy.includes(`<h3 class="test fails js-test-failing">There are errors in beforeAll hook ${emoji}</h3>`)).toBe(true)
  })

  it('draws the passing test count', () => {
    const someQuantity = 3
    const emoji = "&#127881;"

    new Drawer(documentSpy).passingTestCount(someQuantity)

    expect(documentSpy.includes(`<div id="passing-test-count">Test passed: ${someQuantity} ${emoji}</div>`)).toBe(true)
  })

  it('draws the failing test count', () => {
    const someQuantity = 2
    const emoji = "&#128128;"

    new Drawer(documentSpy).failingTestCount(someQuantity)

    expect(documentSpy.includes(`<div id="failing-test-count">Test failed: ${someQuantity} ${emoji}</div>`)).toBe(true)
  })

  it('draws the skipping test count', () => {
    const someQuantity = 11
    const emoji = "&#x1F634;"

    new Drawer(documentSpy).skippingTestCount(someQuantity)

    expect(documentSpy.includes(`<div id="skipping-test-count">Test skipped: ${someQuantity} ${emoji}</div>`)).toBe(true)
  })

  describe('when there are no tests executed', () => {
    it('draws the instructions for create the first test', () => {

      new Drawer(documentSpy).drawInstructionsForCreateATest()

      expect(documentSpy.includes('🖊️ How create your first test:')).toBe(true)
      expect(documentSpy.includes('1º</span> Create a file called <span class="instructions-higthlight">"Tests.js"</span> in the same folder where you have <span class="instructions-higthlight">"Verano.html"</span>')).toBe(true)
      expect(documentSpy.includes('2º</span> Copy the following code in <span class="instructions-higthlight">"Tests.js"</span> file:')).toBe(true)
      expect(documentSpy.includes('describe("Tests", () => {')).toBe(true)
      expect(documentSpy.includes('it("fails", () => {')).toBe(true)
      expect(documentSpy.includes('expect(true).toBe(false)')).toBe(true)
      expect(documentSpy.includes('})')).toBe(true)
    })
  })

  describe('when syntax error is thrown' , () => {
    const emoji = "&#9997;"
    const syntaxErrorMessage =`
        <div class="syntax-error">There are a syntax error. ${emoji}</div>
    `

    it('draws it', () => {

      new Drawer(documentSpy).syntaxError()
      expect(documentSpy.html()).toBe(syntaxErrorMessage)
    })

    it('remove previous draws', () => {
      const drawer = new Drawer(documentSpy)
      drawer.testFailing('some test description', error())

      drawer.syntaxError()

      expect(documentSpy.html()).toBe(syntaxErrorMessage)
    })

    it('does not draw anything more', () => {
      const drawer = new Drawer(documentSpy)

      drawer.syntaxError()

      drawer.testFailing('some test description', error())
      expect(documentSpy.html()).toBe(syntaxErrorMessage)
    })
  })

  function error () {
    try {
      throw new ActualAndExpectedValueDoNotMatchError('actual', 'expected')
    } catch (error) {
      return error
    }
  }
})
