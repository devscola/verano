import { Drawer } from "../src/lib/presentation/Drawer.js"
import { DrawerSpy } from "./support/DrawerSpy.js"

describe('beforeAll', () => {
  let hasBeenExecuted = false

  beforeAll(() => {
    hasBeenExecuted = true
  })

  it('executes at the beginning of the current suit', () => {
    expect(hasBeenExecuted).toBe(true)
    hasBeenExecuted = false
  })

  it('is not executed again', () => {
    expect(hasBeenExecuted).toBe(false)
  })
})

describe('beforeAll failures', () => {
  drawer = new DrawerSpy()

  beforeAll(() => {
    fail()
  })

  afterAll(() => {
    drawer.disableSpy()
  })

  it('are drawn (beforeAll failures)', () => {
    const failure = Drawer.ERROR_BEFORE_ALL_MESSAGE

    expect(drawer.hasDraw(failure)).toBe(true)
  })
})
