# Verano

Testing library that works on most S.O. and helps you with your first steps in TDD, it is easy to use, light and fast.

## System requirements

- `Docker version 20.10.18` or compatible.
- `Docker Compose version v2.3.3` or compatible.

## How to run tests

First of all you have to build and run the project by using the following command:

`sh scripts/up-d.sh`

After run the project unit tests you can run the test opening in your browser the URL:

`http://localhost:8000/TestRunner.html`

For run CI tests you can use the command:

`sh scripts/test.sh`

## Building the artifact

After pass the CI it builds the downloadable artifact that we call "production" and you can download [here](https://gitlab.com/devscola/verano/-/jobs/artifacts/master/download?job=build_job).
