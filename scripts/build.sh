#!/bin/sh

mkdir Verano &&
docker-compose exec -T verano npm run parcel-build &&
docker-compose exec -T verano npm run inline &&
echo '<script src="Tests.js"></script>' >> Verano/Verano.html
